# CS 411 Project :)

### `app` contains the code for the Android application.
### `server` contains the code for the server.

The server will hold the database and allow queries/updating, while the mobile app will make send commands to the server to retrieve or add information.

## Install Instructions

### SSH Keys for GitLab
1. Instructions [here](https://gitlab.engr.illinois.edu/help/ssh/README)
2. If you put your private key in `~\.ssh\` or `C:\Users\Name\.ssh\` it should be found automatically by git.

### Git
1. Install git from [here](https://git-scm.com/downloads)
2. Choose all defaults
3. One time setup, run
  * `git config --global user.name "John Doe"`
  * `git config --global user.email johndoe@example.com`
3. Read [this](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository) on git basics.
  * TL;DR `git add` adds a file to be staged, `git commit` commits staged files, `git status` shows the status of files, `git push` pushes your local changes to GitLab.
4. Read [this](https://git-scm.com/book/en/v1/Git-Branching-What-a-Branch-Is) and [this](https://git-scm.com/book/en/v1/Git-Branching-Basic-Branching-and-Merging) on branching. These are important.
  * TL;DR `git checkout -b branchName`, ..make changes.., `git add changedFile.txt`, `git commit -m "made changes"`, `git push`. To make changes to a branch. 
  * TL;DR `git checkout master`, `git merge branchName`, `git push`. To merge a branch to the master branch. **_Only do this if your code compiles_**.
  * TL;DR `git pull` to try to update your local copy of the branch you're working on (last one you checked out) with the latest from GitLab. Might fail if your copy does not equal a commit (i.e. you made uncomitted changes).

##### Basically, the git workflow should be to make a new branch, add your features, then push and merge the branch with master.

### Android SDK
1. Download and install sdk tools only from [here](https://developer.android.com/studio/index.html#downloads).
2. Unpack the tools somewhere into `/folder` so you have `/folder/tools`. Do this anywhere but where you're going to unpack the actual project repository - your default program install location works well.
3. Set your ANDROID_HOME environment variable to the location of `/folder`.
4. Run `/folder/tools/android sdk` to open up the SDK Manager GUI. (run the `android` file located in tools with the parameter sdk from a terminal).
5. Deselect All
6. Check Tools/Android SDK Tools, Tools/Android SDK Platform-tools, Tools/Android SDK Builds-tools (The latest version)
7. Check Android 5.0.1/SDK Platform
8. Check Extras/Android Support Repository, Extras/Android Support Library, Extras/Google Play Services
9. Install all the packages (will take a while)

### Getting the Project into IntelliJ
1. Download the project as a zip
2. Unpack it so your folder structure is
  * folder
      * app
      * server
      * README.md
3. Open Intellij and choose Import Project
4. Choose `folder/app/TagAlong/build.gradle` as the file to import.
  * If a popup opens asking what to add to gradle, choose both things (the default)
  * If a popup in the bottom right opens up asking about android config, choose configure and then add (the default)
5. Click the button to the left of 'app'  (the run box in top right, should have 'app' as a thing to run) that has the 1/0s
6. Click the green play button to the right of 'app' (the run box in top right, should have 'app' as a thing to run)
  * If it complains about android sdk, set ANDROID_HOME environment variable to your android sdk install OR
  * create a file called local.properties in 'folder/app/TagAlong' and add the line `sdk.dir=<install location>`
      * Special characters need to be escaped if you do this (for example mine is `sdk.dir=C\:\\Users\\Dan\\AppData\\Local\\Android\\android-sdk`)
7. Open up a command prompt and `cd` into `folder`
8. `git clone --no-checkout git@gitlab.engr.illinois.edu:volchek2/411-project.git`
9. Open up a file explorer and move `folder/411-project/.git` to `folder/.git` (move the .git folder one directory up)
10. Delete `folder/411-project`
11. In command prompt at `folder`, run `git reset --hard HEAD`.
12. Intellij will detect an unregistered VCS root, click the popup or go to Settings -> Version Control -> select the unregistered root and click the green +

#### Everything should be set up!