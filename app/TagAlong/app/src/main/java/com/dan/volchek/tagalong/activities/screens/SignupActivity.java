package com.dan.volchek.tagalong.activities.screens;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.ToRidesActivity;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends ToRidesActivity {
    private EditText usernameField;
    private EditText passwordField;
    private EditText contactField;

    private TextView userError;
    private TextView passError;
    private TextView contactError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginDetailsLocation = getFilesDir() + "/details.txt";
        setContentView(R.layout.activity_signup);

        Button signUp = (Button) findViewById(R.id.signupButton);
        usernameField = (EditText) findViewById(R.id.usernameEdit);
        passwordField = (EditText) findViewById(R.id.passwordEdit);
        contactField = (EditText) findViewById(R.id.su_contact);
        userError = (TextView) findViewById(R.id.usernameError);
        passError = (TextView) findViewById(R.id.passwordError);
        contactError = (TextView) findViewById(R.id.contactError);

        ClickListener listener = new ClickListener();
        signUp.setOnClickListener(listener);
    }

    private class ClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            validateSignUpInfo();
        }
    }

    private void validateSignUpInfo() {
        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();
        String contact = contactField.getText().toString();

        String[] okay = Utils.validateUserPass(username, password);
        int[] visib = Utils.validityToViewType(okay);

        boolean contactOK = contact.trim().length() > 0;
        if (contactOK) {
            contactError.setVisibility(View.INVISIBLE);
        } else {
            contactError.setText("Contact info must not be empty");
            contactError.setVisibility(View.VISIBLE);
        }

        userError.setText(okay[0]);
        userError.setVisibility(visib[0]);
        passError.setText(okay[1]);
        passError.setVisibility(visib[1]);

        if (okay[0] == null && okay[1] == null && contactOK) {

            Map<String, String> params = new HashMap<>();
            params.put("request", "signup");
            params.put("username", username);
            params.put("password", password);
            params.put("contact_info", contact.trim());

            JSONObject request = (JSONObject) JSONObject.wrap(params);
            Log.e("REQUEST", request.toString());
            Utils.constructRequest(request, this, this);

            this.username = username;
        }

    }
}

