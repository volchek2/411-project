package com.dan.volchek.tagalong.activities;

import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dan.volchek.tagalong.Utils;
import org.json.JSONObject;

public abstract class VolleyErrorActivity extends CustomFontActivity implements Response.ErrorListener, Response.Listener<JSONObject> {
    @Override
    public void onErrorResponse(VolleyError error) {
        if (error.getMessage() == null)
            Log.e("Error response", "MESSAGE WAS NULL");
        else
            Log.e("Error response", error.getMessage());
        error.printStackTrace();
        Utils.makeToast(this, "Network Error");
    }

}
