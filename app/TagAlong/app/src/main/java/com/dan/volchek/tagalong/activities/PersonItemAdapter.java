package com.dan.volchek.tagalong.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.dan.volchek.tagalong.R;

import java.util.List;

public class PersonItemAdapter extends ArrayAdapter<PersonData> {
    private Context c;

    public PersonItemAdapter(Context context, int resource, List<PersonData> items) {
        super(context, resource, items);
        c = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.person_item, null);
        }

        PersonData p = getItem(position);

        if (p != null) {
            TextView name = (TextView) v.findViewById(R.id.pi_name);
            TextView contact = (TextView)v.findViewById(R.id.pi_info);

            name.setText(p.getUserName());
            contact.setText(p.getContactInfo());
            //icon.setImageURI(p.getIconURI());
        }

        return v;
    }

}