package com.dan.volchek.tagalong.activities.screens;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import com.dan.volchek.tagalong.Graph.Graph;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.CommentData;
import com.dan.volchek.tagalong.activities.CommentItemAdapter;
import com.dan.volchek.tagalong.activities.VolleyErrorActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Dmitriy on 3/3/2017.
 */
public class ProfileActivity extends VolleyErrorActivity {
    private CommentItemAdapter comments;
    private ArrayList<CommentData> commenting;
    private ListView comment_list;
    private String user;

    @Override
    public void onResponse(JSONObject response) {
        //read comments from JSON and insert them
        try {
            String status = response.getString("status");
            String responseString = response.getString("response");
            String request = response.getString("request");
            if (!status.equals("1")) {
                Utils.makeToast(this, responseString); //failure
            } else {
                //on success
                if (request.equals("get_comments_about")) {
                    commenting.clear();
                    JSONArray commentsFromDB = response.getJSONArray("response");
                    createCommentData(commentsFromDB, commenting);
                    comments.notifyDataSetChanged();
                } else if (request.equals("leave_comment") || request.equals("delete_comment")) {
                    getComments();
                } else if (request.equals("contact_info")) {
                    ((TextView) findViewById(R.id.profile_contact)).setText(response.getJSONObject("response")
                            .getString("contact_info"));
                }
            }
        } catch (JSONException e) {
            Log.e("Response", "Data in wrong format?");
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        commenting = new ArrayList<>();
        comment_list = (ListView) findViewById(R.id.profile_commentList);
        String[] data = getIntent().getStringArrayExtra("data");
        //load username and then comments
        user = data[0];
        ((TextView) findViewById(R.id.profile_un)).setText(user);

        String[] deets = Utils.getDetails();
        Map<String, String> params = new HashMap<>();
        params.put("request", "contact_info");
        params.put("auth_token", deets[1]);
        params.put("username", deets[0]);
        params.put("comment_username", user);
        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Utils.constructRequest(request, this, this);

        //http://stackoverflow.com/questions/5056734/android-force-edittext-to-remove-focus
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        getComments();
        Button submit = (Button) findViewById(R.id.profile_comment_submit); //leave_comment = request name
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submitRequestHelper();
            }
        });
        final Activity act = this;
        final ProfileActivity pact = this;
        comment_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                final CommentData d = commenting.get(i);
                if (d.getLeftUserName().equals(Utils.getDetails()[0])) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(act);
                    builder.setMessage("Delete this comment?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    String[] deets = Utils.getDetails();
                                    Map<String, String> params = new HashMap<>();
                                    params.put("request", "delete_comment");
                                    params.put("auth_token", deets[1]);
                                    params.put("username", deets[0]);
                                    params.put("time_left", d.getTimeLeft());
                                    JSONObject request = (JSONObject) JSONObject.wrap(params);
                                    Utils.constructRequest(request, pact, pact);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });
                    // Create the AlertDialog object and return it
                    builder.create().show();
                    return true;

                } else {
                    Utils.startActivity(act, ProfileActivity.class, new String[]{d.getLeftUserName()}, false);
                }
                return false;
            }
        });
        comments = new CommentItemAdapter(this, R.layout.comment_item, commenting);
        comment_list.setAdapter(comments);
        comments.setActivity(this);

    }

    private void getComments() {
        String[] deets = Utils.getDetails();
        Map<String, String> params = new HashMap<>();
        params.put("request", "get_comments_about");
        params.put("auth_token", deets[1]);
        params.put("username", deets[0]);
        params.put("comment_username", user);
        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Utils.constructRequest(request, this, this);
    }

    private void createCommentData(JSONArray commentsJSON, ArrayList<CommentData> a) throws JSONException {
        for (int i = 0; i < commentsJSON.length(); i++) {
            JSONArray comment_info = commentsJSON.getJSONArray(i);
            CommentData datum = new CommentData((String) comment_info.get(0), (String) comment_info.get(1), (String) comment_info.get(2),
                    (String) comment_info.get(3), Integer.parseInt((String) comment_info.get(4)));

            a.add(datum);
        }
    }

    private void submitRequestHelper() {
        Map<String, String> params = new HashMap<>();
        params.put("request", "leave_comment");
        params.put("auth_token", Utils.getDetails()[1]);
        params.put("username", Utils.getDetails()[0]);
        params.put("time_left", "" + Graph.format.format(new Date()));
        params.put("comment_username", user);
        params.put("message", ((TextView) findViewById(R.id.profile_leaveComment)).getText().toString());
        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Utils.constructRequest(request, this, this);
        ((EditText) findViewById(R.id.profile_leaveComment)).setText("");


    }
}