package com.dan.volchek.tagalong.apis;

import android.app.Activity;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.interfaces.GotPosition;
import com.dan.volchek.tagalong.interfaces.PositionResponse;
import com.dan.volchek.tagalong.requests.PositionJsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;//40.102941,-88.220394)

public class MapsApi implements Response.ErrorListener, Response.Listener<JSONObject>, PositionResponse {

    public static final String[] STATES = {"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI",
            "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO",
            "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI",
            "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};

    public void getNameOfLatLng(LatLng pos, GotPosition caller, Activity act_caller) {
        PositionJsonObjectRequest r = new PositionJsonObjectRequest(Request.Method.POST,
                "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + pos.latitude + ","
                        + pos.longitude + "&key=AIzaSyC9O_rv7YsJkJQ4B43Db9vqo8p-FIWblbg",
                null, this, this, caller, act_caller, this);
        Utils.directToQueue(r);

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {

    }

    @Override
    public void fancyResponse(JSONObject response, GotPosition caller, Activity act_caller) {
        Log.e("poopy", response.toString());
        JSONArray results;
        try {
            results = response.getJSONArray("results");
        } catch (JSONException e) {
            Log.e("Maps error", "Api returned bad thingo");
            caller.gotPositionName(null);
            return;
        }
        for (int i = 0; i < results.length(); i++) {
            String address;
            try {
                address = results.getJSONObject(i).getString("formatted_address");
            } catch (JSONException e) {
                Log.e("Maps error", "Response object in bad format?");
                continue;
            }
            String found = null;
            for (String state : STATES)
                if (address.contains(" " + state)) {
                    found = " " + state;
                    break;
                }
            if (found == null)
                continue;
            String front;
            if (address.indexOf(found) + 3 >= address.length())
                front = address;
            else
                front = address.substring(0, address.indexOf(found) + 3);

            int firstComma = front.indexOf(",");
            int secondComma = front.lastIndexOf(",");
            if (firstComma == secondComma) {
                caller.gotPositionName(front);
                return;
            } else {
                caller.gotPositionName(front.substring(firstComma + 1).trim());
                return;
            }
        }

        caller.gotPositionName(null);
    }

    @Override
    public void fancyError(VolleyError error, GotPosition caller, Activity act_caller) {
        if (error.getMessage() == null)
            Log.e("Error response", "MESSAGE WAS NULL");
        else
            Log.e("Error response", error.getMessage());
        error.printStackTrace();
        Utils.makeToast(act_caller, "Network Error");
        caller.gotPositionName(null);
    }
}
