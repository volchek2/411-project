package com.dan.volchek.tagalong.activities;

/**
 * Represents a row in the trips database.
 */
public class RideData {
    private String userName;
    private String startTime;
    private String endTime;
    private String tripName;
    private String start_loc;
    private String end_loc;
    private long cost;
    private double distance;

    public RideData(String name, String stime, String etime, String tripName, String start, String end, long cost,
                    double distance) {
        userName = name;
        startTime = stime;
        endTime = etime;
        this.tripName = tripName;
        start_loc = start;
        end_loc = end;
        this.cost = cost;
        this.distance = distance;
    }

    public String getUserName() {
        return userName;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getTripName() {
        return tripName;
    }

    public String getStartLoc() {
        return start_loc;
    }

    public String getEndLoc() {
        return end_loc;
    }

    public void swap() {
        String temp = start_loc;
        start_loc = end_loc;
        end_loc = temp;
    }

    public long getCost() {
        return cost;
    }

    public double getDistance() {
        return distance;
    }
}
