package com.dan.volchek.tagalong.activities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommentItemAdapter extends ArrayAdapter<CommentData> implements Response.ErrorListener, Response.Listener<JSONObject> {
    private Context c;
    private Activity act;

    public CommentItemAdapter(Context context, int resource, List<CommentData> items) {
        super(context, resource, items);
        c = context;
    }

    public void setActivity(Activity a) {
        act = a;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.comment_item, null);
        }

        final CommentData p = getItem(position);
        final CommentItemAdapter cc = this;

        if (p != null) {
            //set stuff and get stuff once xml is done
            ((TextView) (v.findViewById(R.id.comment_author))).setText(p.getLeftUserName());
            ((TextView) (v.findViewById(R.id.comment_message))).setText(p.getMessage());
            ((TextView) (v.findViewById(R.id.comment_upvotes))).setText(p.getUpvotes() + "");
            ((TextView) (v.findViewById(R.id.comment_timestamp))).setText(p.getTimeLeft());

            ImageView up = (ImageView) v.findViewById(R.id.comment_upvote);
            ImageView down = (ImageView) v.findViewById(R.id.comment_downvote);

            up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Map<String, String> params = new HashMap<>();
                    params.put("request", "upvote_comment");
                    params.put("auth_token", Utils.getDetails()[1]);
                    params.put("username", Utils.getDetails()[0]);
                    params.put("time_left", "" + p.getTimeLeft());
                    params.put("comment_username", p.getLeftUserName());
                    JSONObject request = (JSONObject) JSONObject.wrap(params);
                    Utils.constructRequest(request, cc, cc);
                }
            });

            down.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Map<String, String> params = new HashMap<>();
                    params.put("request", "downvote_comment");
                    params.put("auth_token", Utils.getDetails()[1]);
                    params.put("username", Utils.getDetails()[0]);
                    params.put("time_left", "" + p.getTimeLeft());
                    params.put("comment_username", p.getLeftUserName());
                    JSONObject request = (JSONObject) JSONObject.wrap(params);
                    Utils.constructRequest(request, cc, cc);
                }
            });
        }

        return v;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (act != null)
            Utils.makeToast(act, "Network error voting on comment");
    }

    @Override
    public void onResponse(JSONObject resp) {
        try {
            String status = resp.getString("status");
            if (status.equals("0")) {
                Utils.makeToast(act, resp.getString("response"));
            } else {
                JSONObject response = resp.getJSONObject("response");
                String about = response.getString("username");
                String time_left = response.getString("time_left");
                int votes = response.getInt("votes");
                for (int i = 0; i < getCount(); i++) {
                    CommentData c = getItem(i);
                    if (c.getLeftUserName().equals(about) && c.getTimeLeft().equals(time_left)) {
                        c.setUpvotes(votes);
                        notifyDataSetChanged();
                        break;
                    }


                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
