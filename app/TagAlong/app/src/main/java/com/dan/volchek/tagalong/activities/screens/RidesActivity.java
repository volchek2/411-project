package com.dan.volchek.tagalong.activities.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.RideData;
import com.dan.volchek.tagalong.activities.RideItemAdapter;
import com.dan.volchek.tagalong.activities.VolleyErrorActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RidesActivity extends VolleyErrorActivity {
    private ArrayList<RideData> driving;
    private ArrayList<RideData> riding;
    private ListView riding_in;
    private ListView driving_in;

    private RideItemAdapter rides;
    private RideItemAdapter drives;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rides);

        driving = new ArrayList<>();
        riding = new ArrayList<>();
        riding_in = (ListView) findViewById(R.id.ra_riding_in);
        driving_in = (ListView) findViewById(R.id.ra_driving_in);

        driving_in.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                RideData item = driving.get(pos);
                String[] data = new String[]{item.getUserName(), item.getStartTime(), item.getEndTime(),
                        item.getTripName(), item.getStartLoc(), item.getEndLoc(), "" + item.getCost()};
                Utils.startActivity(activity, RideViewActivity.class, data, false);
            }
        });

        riding_in.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                RideData item = riding.get(pos);
                String[] data = new String[]{item.getUserName(), item.getStartTime(), item.getEndTime(),
                        item.getTripName(), item.getStartLoc(), item.getEndLoc(), "" + item.getCost()};
                Utils.startActivity(activity, RideViewActivity.class, data, false);
            }
        });

        ActionBar bar = getSupportActionBar();
        bar.setTitle(Utils.getDetails()[0] + " - My Rides");

        rides = new RideItemAdapter(this, R.layout.ride_item, riding);
        drives = new RideItemAdapter(this, R.layout.ride_item, driving);

        riding_in.setAdapter(rides);
        driving_in.setAdapter(drives);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    private void updateList() {
        String[] details = Utils.getDetails();
        Log.e("rides", details[0] + " - " + details[1]);
        String username = details[0];
        String authToken = details[1];

        Map<String, String> params = new HashMap<>();
        params.put("request", "driving");
        params.put("username", username);
        params.put("auth_token", authToken);

        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Log.e("REQUEST", request.toString());
        Utils.constructRequest(request, this, this);

        params.put("request", "riding");

        request = (JSONObject) JSONObject.wrap(params);
        Log.e("REQUEST", request.toString());
        Utils.constructRequest(request, this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.rides_action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_logout:
                Map<String, String> params = new HashMap<>();
                params.put("request", "logout");
                params.put("username", Utils.getDetails()[0]);
                params.put("auth_token", Utils.getDetails()[1]);

                JSONObject request = (JSONObject) JSONObject.wrap(params);
                Log.e("REQUEST", request.toString());
                Utils.constructRequest(request, this, this);
                return true;

            case R.id.action_create_ride:

                intent = new Intent(activity, GetRideDataActivity.class);
                intent.putExtra("type", true);
                startActivity(intent);
                return true;

            case R.id.action_find_ride:
                intent = new Intent(activity, GetRideDataActivity.class);
                intent.putExtra("type", false);
                startActivity(intent);
                return true;

            case R.id.action_view_pending:
                intent = new Intent(activity, ViewPendingRequestsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_maps_vis:
                intent = new Intent(activity, VisualizerActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_my_profile:
                Utils.startActivity(this,ProfileActivity.class,new String[]{Utils.getDetails()[0]},false);

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            String status = response.getString("status");
            String request = response.getString("request");
            if (status.equals("0")) {
                Utils.makeToast(this, response.getString("response"));
                return;
            }
            switch (request) {
                case "logout":
                    String responseString = response.getString("response");
                    if (status.equals("1")) {
                        Utils.makeToast(this, responseString);
                        Utils.writeFile(getFilesDir() + "/details.txt", "");
                        Utils.startActivity(this, LoginActivity.class, null, true);
                    }
                    break;
                case "driving": {
                    driving.clear();
                    JSONArray rideee = response.getJSONArray("response");
                    createRideData(rideee, driving);
                    drives.notifyDataSetChanged();
                    break;
                }
                case "riding": {
                    riding.clear();
                    JSONArray rideee = response.getJSONArray("response");
                    createRideData(rideee, riding);
                    rides.notifyDataSetChanged();
                    break;
                }
            }
        } catch (JSONException e) {
            Log.e("Response", "Data in wrong format?");
            e.printStackTrace();
        }
    }

    private void createRideData(JSONArray rides, ArrayList<RideData> arr) throws JSONException {
        for (int i = 0; i < rides.length(); i++) {
            JSONObject ride = rides.getJSONObject(i);
            JSONArray trip_info = ride.getJSONArray("trip_info");
            RideData data = new RideData((String) trip_info.get(0), (String) trip_info.get(1),
                    (String) trip_info.get(2),
                    (String) trip_info.get(3),
                    (String) trip_info.get(4), (String) trip_info.get(5),
                    trip_info.getInt(10), trip_info.getDouble(11));
            arr.add(data);

        }
    }
}
