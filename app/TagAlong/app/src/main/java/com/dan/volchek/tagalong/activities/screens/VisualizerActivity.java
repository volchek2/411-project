package com.dan.volchek.tagalong.activities.screens;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.VolleyErrorActivity;
import com.dan.volchek.tagalong.apis.MapsApi;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VisualizerActivity extends VolleyErrorActivity implements OnMapReadyCallback {
    private GoogleMap map;
    private boolean mapReady;

    private JSONArray all_trips_data;
    private JSONArray passenger_data;

    private ArrayList<Circle> circleData;

    private HashMap<String, Polygon> statePoints;

    private Pair<ArrayList<Marker>, ArrayList<Polyline>> tripData;

    private ArrayList<Marker> popLocs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mapReady = false;
        setContentView(R.layout.activity_vis);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.vis_map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.visualizer_action_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_people_by_state:
                for (Circle c : circleData)
                    c.setVisible(false);
                for (Marker m : tripData.first)
                    m.setVisible(false);
                for (Polyline p : tripData.second)
                    p.setVisible(false);
                for (Marker m : popLocs)
                    m.setVisible(false);

                generateStatePassengers(passenger_data);
                for (Polygon p : statePoints.values())
                    p.setVisible(true);

                return true;

            case R.id.action_cost_by_state:
                for (Circle c : circleData)
                    c.setVisible(false);
                for (Marker m : tripData.first)
                    m.setVisible(false);
                for (Polyline p : tripData.second)
                    p.setVisible(false);
                for (Marker m : popLocs)
                    m.setVisible(false);

                generateStateCosts(all_trips_data);
                for (Polygon p : statePoints.values())
                    p.setVisible(true);

                return true;

            case R.id.action_length_circles:
                for (Marker m : tripData.first)
                    m.setVisible(false);
                for (Polyline p : tripData.second)
                    p.setVisible(false);
                for (Polygon p : statePoints.values())
                    p.setVisible(false);
                for (Marker m : popLocs)
                    m.setVisible(false);

                for (Circle c : circleData)
                    c.setVisible(true);

                return true;

            case R.id.action_popular_destinations:
                for (Circle c : circleData)
                    c.setVisible(false);
                for (Marker m : tripData.first)
                    m.setVisible(false);
                for (Polyline p : tripData.second)
                    p.setVisible(false);
                for (Polygon p : statePoints.values())
                    p.setVisible(false);

                for (Marker m : popLocs)
                    m.setVisible(true);

                return true;

            case R.id.action_view_all_trips:
                for (Circle c : circleData)
                    c.setVisible(false);
                for (Polygon p : statePoints.values())
                    p.setVisible(false);
                for (Marker m : popLocs)
                    m.setVisible(false);

                for (Marker m : tripData.first)
                    m.setVisible(true);
                for (Polyline p : tripData.second)
                    p.setVisible(true);

                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public Pair<ArrayList<Marker>, ArrayList<Polyline>> generateTripData(JSONArray data) {
        ArrayList<MarkerOptions> markers = new ArrayList<>();
        ArrayList<PolylineOptions> lines = new ArrayList<>();

        try {
            for (int i = 0; i < data.length(); i++) {
                JSONArray tuple = data.getJSONArray(i);
                //if (!markerContains(markers, tuple.getString(4)))
                markers.add(new MarkerOptions().position(new LatLng(tuple.getDouble(6), tuple.getDouble(7))).title(tuple.getString(4)));
                //if (!markerContains(markers, tuple.getString(5)))
                markers.add(new MarkerOptions().position(new LatLng(tuple.getDouble(8), tuple.getDouble(9))).title(tuple.getString(5)));
                lines.add(new PolylineOptions().add(new LatLng(tuple.getDouble(6), tuple.getDouble(7)), new LatLng(tuple.getDouble(8), tuple.getDouble(9))).color(randomColor()));
            }
        } catch (JSONException e) {
            return new Pair<>(null, null);
        }

        ArrayList<Marker> m = new ArrayList<>();
        ArrayList<Polyline> l = new ArrayList<>();
        for (MarkerOptions mp : markers)
            m.add(map.addMarker(mp.visible(false)));
        for (PolylineOptions ll : lines)
            l.add(map.addPolyline(ll.visible(false)));


        return new Pair<>(m, l);

    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            String status = response.getString("status");
            if (!status.equals("1")) {
                Utils.makeToast(this, "Error getting vis data");
                return;
            }
            System.out.println(response.getString("request"));
            if (response.getString("request").equals("all_trips")) {
                JSONArray data = response.getJSONArray("response");
                all_trips_data = data;
                circleData = generateCircles(data);
                tripData = generateTripData(data);
                popLocs = getPopularLocations(data);
            } else if (response.getString("request").equals("passenger_count"))
                passenger_data = response.getJSONArray("response");
        } catch (JSONException e) {
            Utils.makeToast(this, "Error getting vis data");
        }
    }

    private void generateStateCosts(JSONArray data) {
        Map<String, Integer> stateCosts = new HashMap<>();
        Map<String, Integer> stateTups = new HashMap<>();

        for (String state : MapsApi.STATES) {
            stateCosts.put(state, 0);
            stateTups.put(state, 0);
        }
        try {
            for (int i = 0; i < data.length(); i++) {
                JSONArray tuple = data.getJSONArray(i);
                String curState = tuple.getString(4).split(",")[1].trim().toUpperCase();
                stateCosts.put(curState, stateCosts.get(curState) + tuple.getInt(10));
                stateTups.put(curState, stateTups.get(curState) + 1);
            }
        } catch (JSONException e) {
            return;
        }
        double minCost = Double.MAX_VALUE;
        double maxCost = 0.0;
        HashMap<String, Double> avgCostsByState = new HashMap<>();
        for (String state : MapsApi.STATES) {
            double avgCost = stateCosts.get(state) / ((double) stateTups.get(state));
            if (stateTups.get(state) == 0)
                avgCost = 0;
            avgCostsByState.put(state, avgCost);
            if (avgCost > maxCost) maxCost = avgCost;
            if (avgCost < minCost) minCost = avgCost;
        }
        for (String state : MapsApi.STATES) {
            double opacityIndex = (avgCostsByState.get(state) * 1.0) / maxCost;
            Polygon curStatePoly = statePoints.get(state);
            curStatePoly.setFillColor(Color.argb((int) Math.max(.2 * 255, opacityIndex * 255 * 0.8),
                    0, 0, 255));
        }
        //data collected time for polygon fun fun

    }

    private ArrayList<Circle> generateCircles(JSONArray data) {
        ArrayList<Circle> circles = new ArrayList<>();
        HashMap<String, ArrayList<Pair<Double, Pair<Double, Double>>>> sCircleInfo = new HashMap<>();
        HashMap<String, ArrayList<Pair<Double, Pair<Double, Double>>>> eCircleInfo = new HashMap<>();
        for (String state : MapsApi.STATES) {
            sCircleInfo.put(state, new ArrayList<Pair<Double, Pair<Double, Double>>>());
            eCircleInfo.put(state, new ArrayList<Pair<Double, Pair<Double, Double>>>());
        }
        try {
            for (int i = 0; i < data.length(); i++) {
                JSONArray tuple = data.getJSONArray(i);
                String start_state = tuple.getString(4).split(",")[1].trim().toUpperCase();
                String end_state = tuple.getString(5).split(",")[1].trim().toUpperCase();

                double distance = distance(String.valueOf(tuple.get(6)),
                        String.valueOf(tuple.get(7)), String.valueOf(tuple.get(8)), String.valueOf(tuple.get(9)));

                Pair<Double, Pair<Double, Double>> s = new Pair<>(distance,
                        new Pair<>(Double.valueOf(String.valueOf(tuple.get(6))),
                                Double.valueOf(String.valueOf(tuple.get(7)))));

                Pair<Double, Pair<Double, Double>> e = new Pair<>(distance,
                        new Pair<>(Double.valueOf(String.valueOf(tuple.get(8))),
                                Double.valueOf(String.valueOf(tuple.get(9)))));

                sCircleInfo.get(start_state).add(s);
                eCircleInfo.get(end_state).add(e);
            }

            for (String s : sCircleInfo.keySet()) {
                double avg_distance = 0;
                double avg_loc_lat = 0;
                double avg_loc_long = 0;
                ArrayList<Pair<Double, Pair<Double, Double>>> points = sCircleInfo.get(s);
                for (Pair<Double, Pair<Double, Double>> p : points) {
                    avg_distance += p.first;

                    avg_loc_lat += p.second.first;
                    avg_loc_long += p.second.second;

                }
                if (points.size() != 0) {
                    avg_distance /= points.size() * 1.0;
                    avg_loc_lat /= points.size() * 1.0;
                    avg_loc_long /= points.size() * 1.0;
                }
                if (avg_distance != 0) {
                    CircleOptions c = new CircleOptions().center(new LatLng(avg_loc_lat, avg_loc_long)).strokeWidth(0).radius(avg_distance * 10000)
                            .fillColor(Color.argb(123, 255, 0, 0)).visible(false);
                    circles.add(map.addCircle(c));//stateColor(S)
                }
            }

            for (String s : eCircleInfo.keySet()) {
                double avg_distance = 0;
                double avg_loc_lat = 0;
                double avg_loc_long = 0;
                ArrayList<Pair<Double, Pair<Double, Double>>> points = eCircleInfo.get(s);
                for (Pair<Double, Pair<Double, Double>> p : points) {
                    avg_distance += p.first;

                    avg_loc_lat += p.second.first;
                    avg_loc_long += p.second.second;

                }
                if (points.size() != 0) {
                    avg_distance /= points.size() * 1.0;
                    avg_loc_lat /= points.size() * 1.0;
                    avg_loc_long /= points.size() * 1.0;
                }
                if (avg_distance != 0) {
                    CircleOptions c = new CircleOptions().center(new LatLng(avg_loc_lat, avg_loc_long)).strokeWidth(0)
                            .fillColor(Color.argb(123, 0, 0, 255)).radius(avg_distance * 10000).visible(false);
                    circles.add(map.addCircle(c));//stateColor(S)
                }
            }
            return circles;
        } catch (JSONException e) {
            return null;
        }
    }

    private ArrayList<Marker> getPopularLocations(JSONArray data) {
        ArrayList<Marker> popLocs = new ArrayList<>();
        HashMap<String, Integer> seenAmount = new HashMap<>();
        try {
            for (int i = 0; i < data.length(); i++) {
                JSONArray tuple = data.getJSONArray(i);
                String endLoc = tuple.getString(5);
                if (!seenAmount.containsKey(endLoc))
                    seenAmount.put(endLoc, 1);
                else
                    seenAmount.put(endLoc, seenAmount.get(endLoc) + 1);
            }

            for (int i = 0; i < 5; i++) {
                if (seenAmount.size() == 0)
                    break;
                String max = getMax(seenAmount);
                Log.e("locs", "Pop loc: " + max);
                seenAmount.remove(max);
                MarkerOptions m = new MarkerOptions().position(findByName(data, max)).title((i + 1) + ": " + max);
                popLocs.add(map.addMarker(m.visible(false)));
            }
            return popLocs;

        } catch (JSONException e) {
            return null;
        }
    }

    private LatLng findByName(JSONArray data, String name) {
        try {
            for (int i = 0; i < data.length(); i++) {
                JSONArray tuple = data.getJSONArray(i);
                String endLoc = tuple.getString(5);
                // Log.e("find", "found: " + endLoc);
                if (endLoc.equals(name))
                    return new LatLng(tuple.getDouble(8), tuple.getDouble(9));
            }


        } catch (JSONException e) {
            return null;
        }
        Log.e("find", "could not find: " + name);
        return null;
    }

    private String getMax(HashMap<String, Integer> amnts) {
        String max = null;
        for (String s : amnts.keySet())
            if (max == null || amnts.get(s) > amnts.get(max))
                max = s;
        return max;
    }

    private void generateStatePassengers(JSONArray data) {
        //assumes data will be {numLocation,Location}
        Map<String, Integer> statePops = new HashMap<String, Integer>();
        for (String state : MapsApi.STATES) {
            statePops.put(state, 0);
        }
        try {
            for (int i = 0; i < data.length(); i++) {
                JSONArray tuple = data.getJSONArray(i);
                String curState = tuple.getString(1).split(",")[1].trim().toUpperCase();
                statePops.put(curState, statePops.get(curState) + tuple.getInt(0));
                curState = tuple.getString(2).split(",")[1].trim().toUpperCase();
                statePops.put(curState, statePops.get(curState) + tuple.getInt(0));
            }
        } catch (JSONException e) {
            return;
        }
        int maxPop = 0;
        int minPop = Integer.MAX_VALUE;

        for (String s : statePops.keySet()) {
            int stateTravelers = statePops.get(s);
            if (stateTravelers > maxPop) maxPop = stateTravelers;
            if (stateTravelers < minPop) minPop = stateTravelers;
        }
        for (String s : statePops.keySet()) {

            double opacityIndex = statePops.get(s) / ((double) maxPop);
            Polygon curStatePoly = statePoints.get(s);
            curStatePoly.setFillColor(Color.argb((int) Math.max(.2 * 255, opacityIndex * 255 * 0.8),
                    255, 0, 0));
        }

    }

    private int randomColor() {
        return Color.argb(123, (int) (255 * Math.random()), (int) (255 * Math.random()), (int) (255 * Math.random()));

    }

    private double distance(String s_lat, String s_long, String e_lat, String e_long) {
        return Math.sqrt(Math.pow(Double.valueOf(s_lat) - Double.valueOf(e_lat), 2) + Math.pow(Double.valueOf(s_long) - Double.valueOf(e_long), 2));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        mapReady = true;
        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(40.0993109, -88.2496952)));

        HashMap<String, PolygonOptions> s = Utils.makeStatePolygons(this);
        statePoints = new HashMap<>();
        for (String ss : s.keySet()) {
            statePoints.put(ss, map.addPolygon(s.get(ss).visible(false).strokeWidth(0)));
        }

        String username = Utils.getDetails()[0];
        String authToken = Utils.getDetails()[1];
        Map<String, String> params = new HashMap<>();
        params.put("request", "all_trips");
        params.put("username", username);
        params.put("auth_token", authToken);


        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Log.e("REQUEST", request.toString());
        Utils.constructRequest(request, this, this);

        params.remove("request");
        params.put("request", "passenger_count");
        request = (JSONObject) JSONObject.wrap(params);
        Log.e("REQUEST", request.toString());
        Utils.constructRequest(request, this, this);

    }
}
