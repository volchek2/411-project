package com.dan.volchek.tagalong.activities.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.PersonData;
import com.dan.volchek.tagalong.activities.PersonItemAdapter;
import com.dan.volchek.tagalong.activities.VolleyErrorActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RideViewActivity extends VolleyErrorActivity {
    private ArrayList<PersonData> people;
    private ListView passengers;
    private PersonItemAdapter adapter;
    private Button join_button;
    private Button edit_button;

    private String driver_name;
    private String start_time;
    private String end_time;
    private String trip_name;
    private String trip_cost;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_view);

        people = new ArrayList<>();
        passengers = (ListView) findViewById(R.id.rv_passenger_list);
        Intent i = getIntent();
        String[] data = i.getStringArrayExtra("data");
        driver_name = data[0];
        start_time = data[1];
        end_time = data[2];
        trip_name = data[3];
        trip_cost = data[6];

        ((TextView) findViewById(R.id.rv_start_time)).setText(getString(R.string.rv_start_time, data[1]));
        ((TextView) findViewById(R.id.rv_end_time)).setText(getString(R.string.rv_end_time, data[2]));
        ((TextView) findViewById(R.id.rv_trip_name)).setText(data[3]);
        ((TextView) findViewById(R.id.rv_start_loc)).setText(getString(R.string.rv_start_loc, data[4]));
        ((TextView) findViewById(R.id.rv_end_loc)).setText(getString(R.string.rv_end_loc, data[5]));
        ((TextView) findViewById(R.id.rv_trip_cost)).setText(getString(R.string.rv_cost, data[6]));

        join_button = (Button) findViewById(R.id.rv_join_button);
        edit_button = (Button) findViewById(R.id.rv_edit_button);

        String username = Utils.getDetails()[0];

        if (data[0].equals(username))
            join_button.setText("Leave");
        if (data[0].equals(username))
            edit_button.setVisibility(View.VISIBLE);

        adapter = new PersonItemAdapter(this, R.layout.person_item, people);
        passengers.setAdapter(adapter);

        final Context c = this;

        (findViewById(R.id.rv_dv_container)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.startActivity(c, ProfileActivity.class, new String[]{driver_name}, false);
            }
        });

        passengers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                String name = people.get(pos).getUserName();
                Utils.startActivity(c, ProfileActivity.class, new String[]{name}, false);

            }
        });

        ((TextView) findViewById(R.id.rv_ds_name)).setText(driver_name);

        String[] deets = Utils.getDetails();
        Map<String, String> params = new HashMap<>();
        params.put("request", "contact_info");
        params.put("auth_token", deets[1]);
        params.put("username", deets[0]);
        params.put("comment_username", driver_name);
        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Utils.constructRequest(request, this, this);

    }

    protected void onResume() {
        super.onResume();
        downloadPass();
    }

    private void downloadPass() {
        String username = Utils.getDetails()[0];
        String authToken = Utils.getDetails()[1];

        Map<String, String> params = new HashMap<>();
        params.put("request", "passengers");
        params.put("username", username);
        params.put("auth_token", authToken);
        params.put("ride_driver", driver_name);
        params.put("ride_start_time", start_time);
        params.put("ride_end_time", end_time);

        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Log.e("REQUEST", request.toString());
        Utils.constructRequest(request, this, this);

        join_button.setOnClickListener(new ClickListener());
        edit_button.setOnClickListener(new ClickListener());
    }

    private void sendRequest(JSONObject request) {
        Utils.constructRequest(request, this, this);
    }

    private class ClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            if (view == join_button) {
                Map<String, String> params = new HashMap<>();
                params.put("request", join_button.getText().toString().toLowerCase());
                params.put("username", Utils.getDetails()[0]);
                params.put("auth_token", Utils.getDetails()[1]);
                params.put("ride_driver", driver_name);
                params.put("ride_start_time", start_time);
                params.put("ride_end_time", end_time);

                JSONObject request = (JSONObject) JSONObject.wrap(params);
                Log.e("REQUEST", request.toString());

                sendRequest(request);
            } else if (view == edit_button) {
                String[] data = new String[]{start_time, end_time, trip_name, trip_cost};
                Utils.startActivity(activity, EditRideActivity.class, data, false);
            }
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("RESPONSE", response.toString());
        try {
            String status = response.getString("status");
            String request = response.getString("request");
            if (status.equals("0")) {
                Utils.makeToast(activity, response.getString("response"));
                return;
            }
            if (request.equals("contact_info")) {
                ((TextView) findViewById(R.id.rv_ds_info)).setText(response.getJSONObject("response")
                        .getString("contact_info"));
            } else if (request.equals("passengers")) {
                people.clear();
                JSONArray arr = response.getJSONArray("response");
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject curr = (JSONObject) arr.get(i);
                    PersonData p = new PersonData(curr.getString("username"),
                            curr.getString("contact_info"));

                    if (curr.getString("username").equals(Utils.getDetails()[0]))
                        join_button.setText("Leave");
                    people.add(p);
                }
                adapter.notifyDataSetChanged();
                join_button.setVisibility(View.VISIBLE);
            } else {
                if (request.equals("join")) {
                    Utils.makeToast(activity, response.getString("response"));
                } else {
                    if (Utils.getDetails()[0].equals(driver_name)) {
                        Utils.makeToast(activity, "Ride deleted");
                        finish();
                    } else {
                        Utils.makeToast(activity, "Left Ride");
                    }
                }
                downloadPass();
            }
        } catch (JSONException e) {
            Log.e("error", "ride view error: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
