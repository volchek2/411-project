package com.dan.volchek.tagalong.activities;

import android.util.Log;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.screens.RidesActivity;
import org.json.JSONException;
import org.json.JSONObject;

public class ToRidesActivity extends VolleyErrorActivity {
    protected String loginDetailsLocation;
    protected String username;

    @Override
    public void onResponse(JSONObject response) {
        try {
            String status = response.getString("status");
            String authToken = response.getString("response");
            Log.e("toridesresponse", status + " - " + authToken+" - "+username);
            if (!status.equals("1")) {
                Utils.makeToast(this, authToken);
            } else {
                Utils.writeFile(loginDetailsLocation, username + "\n" + authToken);
                String[] data = new String[]{username, authToken};
                Utils.setDetails(data);
                Utils.startActivity(activity, RidesActivity.class, null, true);
            }
        } catch (JSONException e) {
            Log.e("Response", "Data in wrong format?");
            e.printStackTrace();
        }
    }
}
