package com.dan.volchek.tagalong.activities.screens;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dan.volchek.tagalong.MyApplication;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.interfaces.GotGas;
import com.dan.volchek.tagalong.interfaces.GotPosition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONObject;

import java.util.HashMap;

public class MapsTestActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<JSONObject>, OnMapReadyCallback,
        GotPosition, GotGas {
    private boolean mapReady;
    private GoogleMap map;
    private Marker marker;

    private LatLng loc;

    private String type;


    @Override
    public void onResponse(JSONObject response) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (error.getMessage() == null)
            Log.e("Error response", "MESSAGE WAS NULL");
        else
            Log.e("Error response", error.getMessage());
        error.printStackTrace();
        Utils.makeToast(this, "Network Error");
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        mapReady = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_test);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.test_map);
        mapFragment.getMapAsync(this);
        type = getIntent().getStringArrayExtra("data")[0];


    }

    @Override
    public void gotPositionName(String name) {
        Utils.makeToast(this, name);
        //Utils.getGasPrice(name, this, this);

        HashMap<String, String> map = new HashMap<>();
        map.put("type", type);
        map.put("lat", "" + loc.latitude);
        map.put("long", "" + loc.longitude);
        map.put("loc_name", name);
        Log.e("map", "set info!");
        ((MyApplication) getApplication()).setLocData(map);

        finish();
        // Utils.startActivity(this, GetRideDataActivity.class, new String[]{name, loc.latitude + "",
        //         loc.longitude + ""}, true);
    }

    @Override
    public void gotGasPrice(double price) {
        Utils.makeToast(this, "Price: " + price);
    }

    private void mapReady() {
        final Activity a = this;
        LatLng uiuc = new LatLng(40.102941, -88.220394);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(uiuc, 14));
        marker = map.addMarker(new MarkerOptions().position(uiuc).title("Location").draggable(true));

        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng latLng) {
                marker.setPosition(latLng);

            }
        });

        /*map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                //  marker.position(latLng);

            }
        });*/

        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

            }
        });

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                markerClicked(marker);
                return false;
            }
        });

    }

    private void markerClicked(Marker mMarker) {
        marker.setPosition(mMarker.getPosition());
        loc = mMarker.getPosition();

        Utils.getLatLongName(mMarker.getPosition(), this, this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;
        this.mapReady = true;
        mapReady();

    }


}
