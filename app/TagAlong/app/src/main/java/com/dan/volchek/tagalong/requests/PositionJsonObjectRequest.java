package com.dan.volchek.tagalong.requests;

import android.app.Activity;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dan.volchek.tagalong.interfaces.GotPosition;
import com.dan.volchek.tagalong.interfaces.PositionResponse;
import org.json.JSONObject;

public class PositionJsonObjectRequest extends JsonObjectRequest {
    private GotPosition caller;
    private Activity act_caller;
    private final PositionResponse response;

    public PositionJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener,
                                     GotPosition caller, Activity act_caller, PositionResponse response) {
        super(method, url, jsonRequest, listener, errorListener);
        this.caller = caller;
        this.act_caller = act_caller;
        this.response = response;
    }

    protected void deliverResponse(JSONObject response) {
        this.response.fancyResponse(response, caller, act_caller);
    }

    public void deliverError(VolleyError error) {
        this.response.fancyError(error, caller, act_caller);
    }
}
