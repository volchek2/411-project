package com.dan.volchek.tagalong.activities.screens;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.PendingData;
import com.dan.volchek.tagalong.activities.PendingItemAdapter;
import com.dan.volchek.tagalong.activities.VolleyErrorActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewPendingRequestsActivity extends VolleyErrorActivity {
    private ListView pendingRequests;
    private PendingItemAdapter adapter;
    private ArrayList<PendingData> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_requests);
        pendingRequests = (ListView) findViewById(R.id.pend_list_view);
        data = new ArrayList<>();

        adapter = new PendingItemAdapter(this, R.layout.pending_item, data);
        pendingRequests.setAdapter(adapter);

        pendingRequests.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                PendingData item = data.get(pos);
                String[] data = new String[]{item.getJoin_name(), item.getStart_time(), item.getEnd_time(),
                        item.getTrip_name()};
                Utils.startActivity(activity, ConfirmPassActivity.class, data, false);
            }
        });
    }

    private void updateList() {
        String username = Utils.getDetails()[0];
        String authToken = Utils.getDetails()[1];
        Map<String, String> params = new HashMap<>();
        params.put("request", "get_pending");
        params.put("username", username);
        params.put("auth_token", authToken);

        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Log.e("REQUEST", request.toString());
        Utils.constructRequest(request, this, this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        updateList();
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("Asd", response.toString());
        try {
            String status = response.getString("status");
            if (status.equals("0")) {
                Utils.makeToast(this, response.getString("response"));
            }

            JSONArray pendings = response.getJSONArray("response");
            data.clear();
            for (int i = 0; i < pendings.length(); i++) {
                JSONArray curr = (JSONArray) pendings.get(i);
                PendingData p = new PendingData(curr.getString(0), curr.getString(2),
                        curr.getString(1), curr.getString(3), curr.getString(4));
                data.add(p);
            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Log.e("error", "pending view error: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
