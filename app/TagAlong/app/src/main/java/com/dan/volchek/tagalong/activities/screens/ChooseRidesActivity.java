package com.dan.volchek.tagalong.activities.screens;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.dan.volchek.tagalong.Graph.Graph;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.MultiRideItemAdapter;
import com.dan.volchek.tagalong.activities.RideData;
import com.dan.volchek.tagalong.activities.VolleyErrorActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ChooseRidesActivity extends VolleyErrorActivity {
    private ListView matchedRides;
    private MultiRideItemAdapter adapter;
    private ArrayList<ArrayList<RideData>> rides;

    private ListView cheapest;
    private MultiRideItemAdapter cheapestAdapter;
    private ArrayList<ArrayList<RideData>> cheapestRides;


    private String startNode;
    private String endNode;
    private double maxCost;
    private double maxDist;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_rides);

        matchedRides = (ListView) findViewById(R.id.cr_list_view);
        rides = new ArrayList<>();
        adapter = new MultiRideItemAdapter(this, R.layout.multi_ride_item, rides);
        adapter.setActivity(this);
        matchedRides.setAdapter(adapter);

        matchedRides.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                ArrayList<RideData> item = rides.get(pos);


            }
        });

        cheapest = (ListView) findViewById(R.id.cr_cheapest_list);
        cheapestRides = new ArrayList<>();
        cheapestAdapter = new MultiRideItemAdapter(this, R.layout.multi_ride_item, cheapestRides);
        cheapest.setAdapter(cheapestAdapter);
        cheapestAdapter.setActivity(this);
    }

    protected void onResume() {
        super.onResume();
        downloadMatches();
    }

    private void downloadMatches() {
        Map<String, String> params = new HashMap<>();

        String[] data = getIntent().getStringArrayExtra("data");

        params.put("request", "some_trips");
        params.put("username", Utils.getDetails()[0]);
        params.put("auth_token", Utils.getDetails()[1]);
        params.put("ride_start_time", data[0]);
        params.put("ride_end_time", data[1]);
        params.put("above_avg", data[6]);

        startNode = data[2];
        endNode = data[3];
        maxCost = Double.valueOf(data[4]);
        maxDist = Double.valueOf(data[5]);
        //params.put("ride_start_loc", data[2]);
        //params.put("ride_end_loc", data[3]);
        //params.put("ride_cost", data[4]);

        if (startNode == null || endNode == null) {
            findViewById(R.id.choose_cheapest).setVisibility(View.GONE);
            findViewById(R.id.cr_cheapest_list).setVisibility(View.GONE);
        }

        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Log.e("REQUEST", request.toString());
        Utils.constructRequest(request, this, this);


    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            String status = response.getString("status");

            if (status.equals("0")) {
                Utils.makeToast(this, response.getString("response"));
                return;
            }
            rides.clear();
            cheapestRides.clear();

            boolean reverse = startNode == null;

            JSONArray data = response.getJSONArray("response");
            Graph g = generateGraph(data, reverse);
            if (startNode != null && g.getNode(startNode) == null) {
                Utils.makeToast(this, "No applicable trips starting from " + startNode);
                return;
            }
            if (endNode != null && g.getNode(endNode) == null) {
                Utils.makeToast(this, "No applicable trips ending at " + endNode);
                return;
            }
            if (endNode != null && startNode != null) {
                ArrayList<RideData> shortest = g.shortestPath(g.getNode(startNode), g.getNode(endNode), maxCost, maxDist);
                ArrayList<ArrayList<RideData>> t = new ArrayList<>();
                t.add(shortest);
                cheapestRides.addAll(t);
                cheapestAdapter.notifyDataSetChanged();
            }

            ArrayList<ArrayList<RideData>> all;
            if (reverse) {
                all = g.allPaths(g.getNode(endNode), g.getNode(startNode), maxCost, maxDist);
                for (ArrayList<RideData> r : all)
                    for(RideData rr : r)
                        rr.swap();

            } else
                all = g.allPaths(g.getNode(startNode), g.getNode(endNode), maxCost, maxDist);

            rides.addAll(all);


            adapter.notifyDataSetChanged();


        } catch (JSONException e) {
            Log.e("Response", "Data in wrong format?");
            e.printStackTrace();
        }
    }

    /*private int findMatching(ArrayList<ArrayList<RideData>> haystack, ArrayList<RideData> needle) {
        for (int i1 = 0; i1 < haystack.size(); i1++) {
            ArrayList<RideData> path = haystack.get(i1);
            if (path.size() != needle.size())
                continue;
            boolean good = true;
            for (int i = 0; i < path.size(); i++) {
                RideData temp = path.get(i);
                RideData ntemp = needle.get(i);
                if (!temp.getUserName().equals(ntemp.getUserName()) ||
                        !temp.getStartTime().equals(ntemp.getStartTime()) ||
                        !temp.getEndTime().equals(ntemp.getEndTime())) {
                    good = false;
                    i = path.size();
                }
            }
            if (good)
                return i1;
        }
        return -1;
    }

    private void createRideData(JSONArray rides, ArrayList<ArrayList<RideData>> arr) throws JSONException {
        for (int i = 0; i < rides.length(); i++) {
            JSONArray ride = rides.getJSONArray(i);
            RideData data = new RideData((String) ride.get(0), (String) ride.get(1),
                    (String) ride.get(2),
                    (String) ride.get(3),
                    (String) ride.get(4), (String) ride.get(5),
                    ride.getInt(10), ride.getDouble(11));
            arr.add(data);

        }
    }*/

    public static Graph generateGraph(JSONArray data, boolean reverse) throws JSONException {
        Graph g = new Graph();
        for (int i = 0; i < data.length(); i++) {
            JSONArray tuple = data.getJSONArray(i);
            int start = reverse ? 5 : 4;
            int end = reverse ? 4 : 5;

            Log.e("create", "adding ride between " + tuple.getString(start) + " and " + tuple.getString(end));


            RideData d = new RideData(tuple.getString(0), tuple.getString(1),
                    tuple.getString(2), tuple.getString(3), tuple.getString(start),
                    tuple.getString(end), tuple.getInt(10), tuple.getDouble(11));


            g.addEdge(g.getOrCreateNode(tuple.getString(start)), g.getOrCreateNode(tuple.getString(end)), d);

        }
        return g;

    }


}
