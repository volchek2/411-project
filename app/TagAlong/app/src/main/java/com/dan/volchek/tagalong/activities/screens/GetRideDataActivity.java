package com.dan.volchek.tagalong.activities.screens;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.*;
import com.dan.volchek.tagalong.MyApplication;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.VolleyErrorActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class GetRideDataActivity extends VolleyErrorActivity {


    private String startTime;
    private String startDate;


    private String startLocation;
    private String endLocation;

    private Pair<String, String> startLoc;
    private Pair<String, String> endLoc;

    private String cost;

    private boolean isDriver;

    private EditText sTimeText;
    private EditText sDateText;

    private EditText eTimeText;
    private EditText eDateText;

    private EditText startLocEdit;
    private EditText endLocEdit;

    private EditText costEdit;

    private EditText tripNameEdit;

    private EditText maxDistEdit;

    private Button createRideButton;

    private CheckBox highBox;

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH-mm-ss");

    Intent startRideIntent;
    //isDriver = startRideIntent.getBooleanExtra("type",false);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_ride_data);

        sTimeText = (EditText) findViewById(R.id.startTimeEdit);
        sDateText = (EditText) findViewById(R.id.startDateEdit);
        eTimeText = (EditText) findViewById(R.id.endTimeEdit);
        eDateText = (EditText) findViewById(R.id.endDateEdit);

        startLocEdit = (EditText) findViewById(R.id.StartLocationEdit);
        endLocEdit = (EditText) findViewById(R.id.EndLocationEdit);
        costEdit = (EditText) findViewById(R.id.costEdit);
        tripNameEdit = (EditText) findViewById(R.id.grd_trip_name);
        maxDistEdit = (EditText) findViewById(R.id.maxDistEdit);

        highBox = (CheckBox) findViewById(R.id.high_users_box);


        final Context t = this;

        sDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar = Calendar.getInstance();

                new DatePickerDialog(t, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub

                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        sDateText.setText(dateFormat.format(myCalendar.getTime()));
                    }
                }, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        eDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar = Calendar.getInstance();
                new DatePickerDialog(t, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        eDateText.setText(dateFormat.format(myCalendar.getTime()));
                    }
                }, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        sTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar = Calendar.getInstance();
                new TimePickerDialog(t, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int i, int i1) {
                        sTimeText.setText(timeFormat.format(myCalendar.getTime()));
                    }
                }, myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), false).show();
            }
        });

        eTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar = Calendar.getInstance();
                new TimePickerDialog(t, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int i, int i1) {
                        eTimeText.setText(timeFormat.format(myCalendar.getTime()));
                    }
                }, myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), false).show();
            }
        });

        startLocEdit.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Utils.startActivity(t, MapsTestActivity.class, new String[]{"start"}, false);
                return true;
            }
        });

        endLocEdit.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Utils.startActivity(t, MapsTestActivity.class, new String[]{"end"}, false);
                return true;
            }
        });


        startRideIntent = getIntent();
        isDriver = startRideIntent.getBooleanExtra("type", false);
        createRideButton = (Button) findViewById(R.id.finishButton);
        ActionBar bar = getSupportActionBar();
        if (isDriver) {
            costEdit.setHint("Cost Per Person");
            createRideButton.setText("Create Ride");
            bar.setTitle("Create A Ride");
            maxDistEdit.setVisibility(View.INVISIBLE);
            highBox.setVisibility(View.INVISIBLE);
        } else {
            createRideButton.setText("Look for Rides");
            bar.setTitle("Find Rides");
            tripNameEdit.setVisibility(View.INVISIBLE);
        }


        ClickListener listener = new ClickListener();
        createRideButton.setOnClickListener(listener);

    }

    protected void onResume() {
        super.onResume();
        Log.e("resumed", "aaaaaaaaaaa");
        HashMap<String, String> info = ((MyApplication) getApplication()).getLocData();
        //Log.e("resumed",info);
        if (info == null)
            Log.e("resumed", "info is null");
        else
            Log.e("resumed", info.get("type"));
        if (info != null) {
            if (info.get("type").equals("start")) {
                startLocEdit.setText(info.get("loc_name"));
                startLoc = new Pair<>(info.get("lat"), info.get("long"));
            } else {
                endLocEdit.setText(info.get("loc_name"));
                endLoc = new Pair<>(info.get("lat"), info.get("long"));
            }
        }
        ((MyApplication) getApplication()).setLocData(null);
    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            String status = response.getString("status");
            Utils.makeToast(this, response.getString("response"));
            if (status.equals("1"))
                finish();//need to go back one more activity

        } catch (JSONException e) {
            Log.e("Response", "Data in wrong format?");
            e.printStackTrace();
        }
    }

    private void handleClick() {

        //Basically just fill in values for all the stuff we need:

        //Start Time and Date:
        startTime = sTimeText.getText().toString().trim();
        if (startTime.equals(""))
            startTime = "00:00:00";

        startDate = sDateText.getText().toString().trim();
        if (startDate.equals(""))
            startDate = "2000-01-01";

        startTime = startDate + " " + startTime;

        String endDate = eDateText.getText().toString().trim();
        String endT = eTimeText.getText().toString().trim();
        if (endDate.equals(""))
            endDate = "3000-01-01";
        if (endT.equals(""))
            endT = "23:59:59";

        String endTime = endDate + " " + endT;

        //End time will be decided by a Google Maps query?

        //Start Location:
        startLocation = startLocEdit.getText().toString().trim();
        if (startLocation.equals(""))
            startLocation = null;

        //End Location:
        endLocation = endLocEdit.getText().toString().trim();
        if (endLocation.equals(""))
            endLocation = null;

        //Cost:
        cost = costEdit.getText().toString().trim();
        if (cost.equals(""))
            cost = "-1";

        String maxDist = maxDistEdit.getText().toString().trim();
        if (maxDist.equals(""))
            maxDist = "-1";

        String name = tripNameEdit.getText().toString().trim();

        String[] data = new String[]{startTime, endTime, startLocation, endLocation, cost, maxDist, highBox.isChecked() + ""};
        if (isDriver) {
            Map<String, String> params = new HashMap<>();
            String[] deets = Utils.getDetails();

            params.put("request", "create_ride");
            params.put("username", deets[0]);
            params.put("auth_token", deets[1]);
            params.put("ride_start_time", startTime);
            params.put("ride_end_time", endTime);
            params.put("ride_start_loc", startLocation);
            params.put("ride_end_loc", endLocation);
            params.put("ride_start_loc_lat", startLoc.first);
            params.put("ride_start_loc_long", startLoc.second);
            params.put("ride_end_loc_lat", endLoc.first);
            params.put("ride_end_loc_long", endLoc.second);
            params.put("ride_cost", cost);
            params.put("ride_name", name);
            params.put("ride_distance", haversine(Double.valueOf(startLoc.first), Double.valueOf(startLoc.second),
                    Double.valueOf(endLoc.first), Double.valueOf(endLoc.second)) + "");

            JSONObject request = (JSONObject) JSONObject.wrap(params);
            Log.e("REQUEST", request.toString());
            Utils.constructRequest(request, this, this);
        } else {
            /*Send to the activity that lets users choose rides to request*/
            Utils.startActivity(activity, ChooseRidesActivity.class, data, false);
        }

    }

    private static double haversine(double s_lat, double s_long, double e_lat, double e_long) {
        double s_lat_rad = Math.toRadians(s_lat);
        double e_lat_rad = Math.toRadians(e_lat);

        double delta_lat = Math.toRadians(e_lat - s_lat);
        double delta_long = Math.toRadians(e_long - s_long);

        double f = Math.pow(Math.sin(delta_lat / 2.0), 2) + Math.cos(s_lat_rad) * Math.cos(e_lat_rad) *
                Math.pow(Math.sin(delta_long / 2.0), 2);
        double g = 2 * Math.atan2(Math.sqrt(f), Math.sqrt(1 - f));
        return 6371 * g;
    }

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            handleClick();
        }
    }


}
