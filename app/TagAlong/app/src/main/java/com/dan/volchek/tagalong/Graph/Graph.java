package com.dan.volchek.tagalong.Graph;

import android.util.Log;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.RideData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Graph {
    private ArrayList<Node> nodes;

    public static void main(String[] args) {
        Graph g = new Graph();

        RideData d = new RideData("driver", "2017-08-08 11:00:00",
                "2017-08-08 21:00:00", "trip", "Chicago, IL",
                "Champaign, IL", 12, 5);

        g.addEdge(g.getOrCreateNode("Chicago, IL"), g.getOrCreateNode("Champaign, IL"), d);

        d = new RideData("driver", "2017-08-08 11:00:00",
                "2017-08-08 21:00:00", "chic->poop", "Chicago, IL",
                "Poopo", 20, 2);

        g.addEdge(g.getOrCreateNode("Chicago, IL"), g.getOrCreateNode("poopo"), d);

        d = new RideData("driver", "2017-08-08 22:00:00",
                "2017-08-08 23:00:00", "poop->champ", "poopo",
                "Champaign, IL", 30, 2);

        g.addEdge(g.getOrCreateNode("poopo"), g.getOrCreateNode("Champaign, IL"), d);

        ArrayList<RideData> path = g.shortestPath(g.getNode("Chicago, IL"), g.getNode("Champaign, IL"), -1, -1);
        System.out.println("---");
        for (RideData p : path) {
            System.out.println(p.getTripName());
        }
        ArrayList<ArrayList<RideData>> all = g.allPaths(g.getNode("Chicago, IL"), g.getNode("Champaign, IL"), -1, 5);
        for (ArrayList<RideData> p : all) {
            System.out.println("---");
            for (RideData pp : p)
                System.out.print(pp.getTripName() + ", ");
            System.out.println();
        }
    }

    public void addEdge(Node start, Node end, RideData d) {
        start.adjacent.add(new Edge(d, start, end, d.getDistance()));
    }

    public Graph() {
        nodes = new ArrayList<>();
    }

    public Node getNode(String cityName) {
        if (cityName == null)
            return null;
        for (Node n : nodes)
            if (n.cityName.equals(cityName))
                return n;
        return null;
    }

    public Node getOrCreateNode(String cityName) {
        Node n = getNode(cityName);
        if (n != null)
            return n;
        n = new Node(cityName);
        nodes.add(n);
        return n;
    }


    public ArrayList<RideData> shortestPath(Node start, Node end, double maxCost, double maxDist) {
        ArrayList<Node> unvisited = new ArrayList<>();
        HashMap<Node, Double> cost = new HashMap<>();
        HashMap<Node, Edge> parent = new HashMap<>();

        HashMap<Node, Double> currentTime = new HashMap<>();
        HashMap<Node, Double> currentDist = new HashMap<>();

        for (Node n : nodes) {
            cost.put(n, Double.MAX_VALUE);
            parent.put(n, null);
            currentTime.put(n, Double.MAX_VALUE);
            currentDist.put(n, Double.MAX_VALUE);
            unvisited.add(n);
        }

        cost.put(start, 0.0);
        currentTime.put(start, 0.0);
        currentDist.put(start, 0.0);

        while (unvisited.size() != 0) {
            Node u = getMinIndex(cost, unvisited);
           /* int min = -1;
            for (int i = 0; i < unvisited.size(); i++)
                if (unvisited.get(i).cityName.equals(u.cityName))
                    min = i;*/
            unvisited.remove(u);
            Log.e("disjktra", "=======");
            Log.e("dijsktra", "min city: " + u.cityName);
            //for (Node n : unvisited)
            //    Log.e("dist", n.cityName);
            for (Edge e : u.adjacent) {
                Log.e("disjktra", "testing edge between " + e.startNode.cityName + " and " + e.endNode.cityName);
                Node v = e.endNode;
                double alt = cost.get(u) + e.data.getCost();

                Log.e("disjktra", "Cost is" + cost.get(u) + " + " + e.data.getCost() + " = " + alt);
                Log.e("disjktra", "Old cost is " + cost.get(v));
                try {
                    Log.e("disjktra", "curr time is " + currentTime.get(u) + " and start of e is " + format.parse(e.data.getStartTime()).getTime());
                    Log.e("disjktra", "times are compatible: " + (currentTime.get(u) <= format.parse(e.data.getStartTime()).getTime() ? "yes" : "no"));

                    double newTime = format.parse(e.data.getEndTime()).getTime();
                    double newDist = currentDist.get(u) + e.data.getDistance();
                    if (alt < cost.get(v)
                            && currentTime.get(u) <= format.parse(e.data.getStartTime()).getTime()
                            && (maxCost == -1 || alt <= maxCost)
                            && (maxDist == -1 || newDist <= maxDist)) {

                        parent.put(v, e);
                        currentTime.put(v, newTime);
                        cost.put(v, alt);
                        currentDist.put(v, newDist);
                    }
                } catch (ParseException ee) {
                    ee.printStackTrace();
                    Log.e("dijkstra", "error parsing a time");
                }
            }
        }

        ArrayList<RideData> rides = new ArrayList<>();
        Node curr = end;
        for (Node n : parent.keySet()) {
            Edge e = parent.get(n);
            if (e == null) {
                Log.e("shortest", "no edge goes to " + n.cityName);
            } else {
                Log.e("shortest", e.startNode.cityName + " goes to " + e.endNode.cityName + " (" + n.cityName + ") using " + e.data.getTripName());
            }
        }
        while (curr != start) {
            if (!parent.keySet().contains(curr)) {
                Log.e("shortest", curr.cityName + " has no parent");
                return new ArrayList<>();
            }

            Edge e = parent.get(curr);
            if (e == null) {
                Log.e("shortest", curr.cityName + " has null parent");
                return new ArrayList<>();
            }

            rides.add(e.data);
            Log.e("shortest", "adding " + e.data.getTripName());
            curr = e.startNode;
        }
        Collections.reverse(rides);
        return rides;

    }

    public ArrayList<ArrayList<RideData>> allPaths(Node start, Node end, double maxCost, double maxDist) {
        ArrayList<Boolean> visited = new ArrayList<>();
        ArrayList<RideData> path = new ArrayList<>();
        ArrayList<ArrayList<RideData>> allPaths = new ArrayList<>();
        int i = 0;
        for (Node n : nodes) {
            n.index = i++;
            visited.add(false);
        }
        try {
            allPaths(start, end, null, visited, 0L, 0,
                    0, path, allPaths, maxCost, maxDist);
        } catch (ParseException e) {
            return new ArrayList<>();
        }
        sortRidesByDistance(allPaths);
        return allPaths;
    }

    public void allPaths(Node start, Node end, Edge taken, ArrayList<Boolean> visited,
                         long currTime, double currCost, double currDist,
                         ArrayList<RideData> currPath,
                         ArrayList<ArrayList<RideData>> allPaths, double maxCost, double maxDist) throws ParseException {

        visited.set(start.index, true);
        if (taken != null)
            currPath.add(taken.data);

        if (end != null && start.cityName.equals(end.cityName))
            allPaths.add(deepCopy(currPath));
        else {
            if (end == null && taken != null)
                allPaths.add(deepCopy(currPath));
            for (Edge e : start.adjacent)
                if (!visited.get(e.endNode.index)
                        && currTime <= format.parse(e.data.getStartTime()).getTime()
                        && (maxCost == -1 || currCost + e.data.getCost() <= maxCost)
                        && (maxDist == -1 || currDist + e.data.getDistance() <= maxDist))
                    allPaths(e.endNode, end, e, visited, format.parse(e.data.getEndTime()).getTime(),
                            currCost + e.data.getCost(), currDist + e.data.getDistance(),
                            currPath, allPaths, maxCost, maxDist);
        }
        if (currPath.size() != 0)
            currPath.remove(currPath.size() - 1);
        visited.set(start.index, false);

    }

    private static void sortRidesByDistance(ArrayList<ArrayList<RideData>> rides) {
        HashMap<ArrayList<RideData>, Double> map = new HashMap<>();
        for (ArrayList<RideData> ride : rides)
            map.put(ride, Utils.totalDistance(ride));
        rides.clear();
        while (map.size() != 0) {
            ArrayList<RideData> min = minRide(map);
            map.remove(min);
            rides.add(min);
        }
    }

    private static ArrayList<RideData> minRide(HashMap<ArrayList<RideData>, Double> rides) {
        ArrayList<RideData> min = null;
        for (ArrayList<RideData> ride : rides.keySet())
            if (min == null || rides.get(ride) < rides.get(min))
                min = ride;
        return min;
    }

    private ArrayList<RideData> deepCopy(ArrayList<RideData> target) {
        ArrayList<RideData> ret = new ArrayList<>();
        for (RideData r : target)
            ret.add(new RideData(r.getUserName(), r.getStartTime(), r.getEndTime(),
                    r.getTripName(), r.getStartLoc(), r.getEndLoc(), r.getCost(), r.getDistance()));
        return ret;
    }

    public static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static Node getMinIndex(HashMap<Node, Double> distances, ArrayList<Node> unvisited) {
        Node min_index = null;
        for (Node n : distances.keySet())
            if (unvisited.contains(n) && (min_index == null || distances.get(n) < distances.get(min_index))) {
                min_index = n;
            }
        return min_index;
    }
}
