package com.dan.volchek.tagalong;

import android.app.Application;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import java.util.HashMap;

public class MyApplication extends Application {
    private HashMap<String,String> data;
    @Override
    public void onCreate() {
        super.onCreate();
        Utils.setUpRequestQueue(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Raleway.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
      }

      public void setLocData(HashMap<String,String> d){
        data=d;
      }

      public HashMap<String,String> getLocData(){
          return data;
      }
}
