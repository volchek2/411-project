package com.dan.volchek.tagalong.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.dan.volchek.tagalong.R;

import java.util.List;

public class PendingItemAdapter extends ArrayAdapter<PendingData> {
    private Context c;

    public PendingItemAdapter(Context context, int resource, List<PendingData> items) {
        super(context, resource, items);
        c = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.pending_item, null);
        }

        PendingData p = getItem(position);

        if (p != null) {
            TextView wants = (TextView) v.findViewById(R.id.pendi_join_name);
            TextView name = (TextView) v.findViewById(R.id.pendi_trip);
            wants.setText(p.getJoin_name());
            name.setText(p.getTrip_name());
        }

        return v;
    }

}
