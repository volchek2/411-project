package com.dan.volchek.tagalong.activities.screens;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.VolleyErrorActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditRideActivity extends VolleyErrorActivity {
    private String startTime;
    private String endTime;

    private EditText nameEdit;
    private EditText costEdit;

    private Button changeButton;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ride);
        costEdit = (EditText) findViewById(R.id.re_cost_edit);
        nameEdit = (EditText) findViewById(R.id.re_name_edit);
        changeButton = (Button) findViewById(R.id.re_change_button);
        changeButton.setOnClickListener(new ClickListener());

        Intent data = getIntent();
        String[] info = data.getStringArrayExtra("data");
        startTime = info[0];
        endTime = info[1];

        nameEdit.setText(info[2]);
        costEdit.setText(info[3]);


    }

    private void sendRequest(JSONObject request) {
        Utils.constructRequest(request, this, this);
    }

    private class ClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            Map<String, String> params = new HashMap<>();
            params.put("request", "edit_ride");
            params.put("username", Utils.getDetails()[0]);
            params.put("auth_token", Utils.getDetails()[1]);
            params.put("ride_start_time", startTime);
            params.put("ride_end_time", endTime);
            params.put("ride_name", nameEdit.getText().toString().trim());
            params.put("ride_cost", costEdit.getText().toString().trim());

            JSONObject request = (JSONObject) JSONObject.wrap(params);
            Log.e("REQUEST", request.toString());

            sendRequest(request);
        }
    }


    @Override
    public void onResponse(JSONObject response) {
        Log.e("RESPONSE", response.toString());
        try {
            String status = response.getString("status");
            String request = response.getString("request");
            if (status.equals("0")) {
                Utils.makeToast(activity, response.getString("response"));
                return;
            }
            Utils.makeToast(activity, "Ride Updated");
            finish();
        } catch (JSONException e) {
            Log.e("error", "ride view error: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
