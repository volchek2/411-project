package com.dan.volchek.tagalong.activities;

public class PendingData {
    private String trip_name;
    private String join_name;
    private String driver_name;
    private String start_time;
    private String end_time;

    public PendingData(String tripname, String joinname, String drivername, String starttime, String endtime){
        trip_name=tripname;
        join_name=joinname;
        driver_name=drivername;
        start_time=starttime;
        end_time=endtime;
    }

    public String getTrip_name() {
        return trip_name;
    }

    public String getJoin_name() {
        return join_name;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getEnd_time() {
        return end_time;
    }
}
