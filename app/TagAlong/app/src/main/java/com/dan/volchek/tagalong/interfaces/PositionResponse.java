package com.dan.volchek.tagalong.interfaces;

import android.app.Activity;
import com.android.volley.VolleyError;
import org.json.JSONObject;

public interface PositionResponse {
    void fancyResponse(JSONObject response, GotPosition caller, Activity act_caller);

    void fancyError(VolleyError error, GotPosition caller, Activity act_caller);
}
