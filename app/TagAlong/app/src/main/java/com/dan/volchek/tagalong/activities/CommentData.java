package com.dan.volchek.tagalong.activities;

/**
 * Created by Dmitriy on 3/11/2017.
 */
public class CommentData {
    private String timeLeft;
    private String leftUserName;
    private String aboutUserName;
    private String message;
    private int upvotes;
    public CommentData(String timeL, String leftUN, String aboutUN, String message, int uvotes)
    {
        timeLeft=timeL;
        leftUserName=leftUN;
        aboutUserName=aboutUN;
        this.message=message;
        upvotes=uvotes;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public String getLeftUserName() {
        return leftUserName;
    }

    public String getAboutUserName() {
        return aboutUserName;
    }

    public String getMessage() {
        return message;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(int upvotes){ this.upvotes=upvotes;}
}
