package com.dan.volchek.tagalong.requests;

import android.app.Activity;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dan.volchek.tagalong.interfaces.GasResponse;
import com.dan.volchek.tagalong.interfaces.GotGas;
import org.json.JSONObject;

public class GasJsonObjectRequest extends JsonObjectRequest {
    private GotGas caller;
    private Activity act_caller;
    private final GasResponse response;

    public GasJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener,
                                GotGas caller, Activity act_caller, GasResponse response) {
        super(method, url, jsonRequest, listener, errorListener);
        this.caller = caller;
        this.act_caller = act_caller;
        this.response = response;
    }

    protected void deliverResponse(JSONObject response) {
        this.response.fancyResponse(response, caller, act_caller);
    }

    public void deliverError(VolleyError error) {
        this.response.fancyError(error, caller, act_caller);
    }
}
