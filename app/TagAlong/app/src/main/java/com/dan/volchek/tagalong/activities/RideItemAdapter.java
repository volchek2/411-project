package com.dan.volchek.tagalong.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.dan.volchek.tagalong.R;

import java.util.List;

public class RideItemAdapter extends ArrayAdapter<RideData> {
    private Context c;

    public RideItemAdapter(Context context, int resource, List<RideData> items) {
        super(context, resource, items);
        c = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.ride_item, null);
        }

        RideData p = getItem(position);

        if (p != null) {
            TextView name = (TextView) v.findViewById(R.id.ri_ride_name);
            TextView s_loc = (TextView) v.findViewById(R.id.ri_start_loc);
            TextView e_loc = (TextView) v.findViewById(R.id.ri_end_loc);
            TextView s_time = (TextView) v.findViewById(R.id.ri_start_time);
            TextView e_time = (TextView) v.findViewById(R.id.ri_end_time);

            name.setText(p.getUserName()+" - "+p.getTripName());
            s_loc.setText(p.getStartLoc());
            e_loc.setText(p.getEndLoc());
            s_time.setText(p.getStartTime());
            e_time.setText(p.getEndTime());
        }

        return v;
    }


}