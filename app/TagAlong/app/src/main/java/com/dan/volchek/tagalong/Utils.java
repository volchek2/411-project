package com.dan.volchek.tagalong;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dan.volchek.tagalong.activities.RideData;
import com.dan.volchek.tagalong.apis.GasApi;
import com.dan.volchek.tagalong.apis.MapsApi;
import com.dan.volchek.tagalong.interfaces.GotGas;
import com.dan.volchek.tagalong.interfaces.GotPosition;
import com.dan.volchek.tagalong.requests.MyJsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class Utils {
    private static String[] details;
    private static Pattern userPassPattern = Pattern.compile("\\w+");
    private static RequestQueue queue;
    private static final String serverURL = "http://cstagalong.web.engr.illinois.edu/query/query.php";

    private static MapsApi mapsApi = new MapsApi();
    private static GasApi gasApi = new GasApi();

    private static boolean loadedStatePoints = false;
    private static HashMap<String, PolygonOptions> statePoints = null;
    private static final HashMap<String, String> nameToShortMap = new HashMap<>();

    static void setUpRequestQueue(Context context) {
        queue = Volley.newRequestQueue(context);
    }

    public static void setDetails(String[] det) {
        details = det;
    }

    public static String[] getDetails() {
        return details;
    }

    private static void addToQueue(Request request) {
        request.setShouldCache(false);
        queue.add(request);
    }

    public static void constructRequest(JSONObject parameters, Response.ErrorListener el, Response.Listener<JSONObject> l) {
        Log.e("REQUEST", parameters.toString());
        JsonObjectRequest request = new MyJsonObjectRequest(Request.Method.POST,
                serverURL,
                parameters,
                l, el);
        Utils.addToQueue(request);

    }

    public static void directToQueue(JsonObjectRequest request) {
        Utils.addToQueue(request);
    }

    public static void makeToast(Activity act, String text) {
        Toast.makeText(act, text,
                Toast.LENGTH_LONG).show();
    }

    public static void startActivity(Context packageContext, Class<?> activity, String[] data, boolean removeHistory) {
        Intent intent = new Intent(packageContext, activity);
        if (removeHistory)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("data", data);
        packageContext.startActivity(intent);
    }

    public static String[] validateUserPass(String username, String password) {
        String[] valid = new String[]{null, null};

        if (username.length() < 5 || username.length() > 20)
            valid[0] = "Username must be between 5 and 20 characters.";

        if (password.length() < 5 || password.length() > 20)
            valid[1] = "Password must be between 5 and 20 characters.";

        if (!userPassPattern.matcher(username).matches())
            valid[0] = "Username can only contain alphanumeric characters and underscores";

        if (!userPassPattern.matcher(password).matches())
            valid[1] = "Password can only contain alphanumeric characters and underscores";

        return valid;


    }

    public static int[] validityToViewType(String[] validity) {
        int[] viewType = new int[2];
        viewType[0] = validity[0] == null ? View.INVISIBLE : View.VISIBLE;
        viewType[1] = validity[1] == null ? View.INVISIBLE : View.VISIBLE;
        return viewType;
    }

    public static String readFile(String path) {
        File file = new File(path);
        try {
            FileInputStream fInput = new FileInputStream(file);

            BufferedReader reader = new BufferedReader(new InputStreamReader(fInput));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            return sb.toString();
        } catch (IOException e) {
            return null;
        }
    }

    public static void writeFile(String path, String text) {
        File file = new File(path);
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }

            FileOutputStream fOutput = new FileOutputStream(file);
            fOutput.write(text.getBytes());
            fOutput.flush();
            fOutput.close();
        } catch (IOException e) {
            Log.e("Write File", "Could not write to file - `" + path + "`");
            e.printStackTrace();
        }

    }

    /**
     * Gets the name of a city in the format <City Name>, <2 Letter State Abbreviation>
     *
     * @param pos        A LatLng object as given by the google maps api
     * @param caller     should always be this - class to recieve name
     * @param act_caller should always be this - class to create toasts if errors
     */
    public static void getLatLongName(LatLng pos, GotPosition caller, Activity act_caller) {
        mapsApi.getNameOfLatLng(pos, caller, act_caller);
    }

    /**
     * Gets gas price of city
     *
     * @param name       MUST BE IN FORMAT <City Name>, <2 Letter State Abbreviation> (given by getLatLongName)
     * @param caller     should always be this - class to receive price
     * @param act_caller should always be this - class to create toasts if errors
     */
    public static void getGasPrice(String name, GotGas caller, Activity act_caller) {
        gasApi.getGasPrice(name, caller, act_caller);
    }

    public static HashMap<String, PolygonOptions> makeStatePolygons(Activity act) {
        Log.e("polygons", "started load");
        if (loadedStatePoints)
            return statePoints;

        nameToShortMap.put("ALABAMA", "AL");
        nameToShortMap.put("ALASKA", "AK");
        nameToShortMap.put("ARIZONA", "AZ");
        nameToShortMap.put("ARKANSAS", "AR");
        nameToShortMap.put("CALIFORNIA", "CA");
        nameToShortMap.put("COLORADO", "CO");
        nameToShortMap.put("CONNECTICUT", "CT");
        nameToShortMap.put("DELAWARE", "DE");
        nameToShortMap.put("FLORIDA", "FL");
        nameToShortMap.put("GEORGIA", "GA");
        nameToShortMap.put("HAWAII", "HI");
        nameToShortMap.put("IDAHO", "ID");
        nameToShortMap.put("ILLINOIS", "IL");
        nameToShortMap.put("INDIANA", "IN");
        nameToShortMap.put("IOWA", "IA");
        nameToShortMap.put("KANSAS", "KS");
        nameToShortMap.put("KENTUCKY", "KY");
        nameToShortMap.put("LOUISIANA", "LA");
        nameToShortMap.put("MAINE", "ME");
        nameToShortMap.put("MARYLAND", "MD");
        nameToShortMap.put("MASSACHUSETTS", "MA");
        nameToShortMap.put("MICHIGAN", "MI");
        nameToShortMap.put("MINNESOTA", "MN");
        nameToShortMap.put("MISSISSIPPI", "MS");
        nameToShortMap.put("MISSOURI", "MO");
        nameToShortMap.put("MONTANA", "MT");
        nameToShortMap.put("NEBRASKA", "NE");
        nameToShortMap.put("NEVADA", "NV");
        nameToShortMap.put("NEW HAMPSHIRE", "NH");
        nameToShortMap.put("NEW JERSEY", "NJ");
        nameToShortMap.put("NEW MEXICO", "NM");
        nameToShortMap.put("NEW YORK", "NY");
        nameToShortMap.put("NORTH CAROLINA", "NC");
        nameToShortMap.put("NORTH DAKOTA", "ND");
        nameToShortMap.put("OHIO", "OH");
        nameToShortMap.put("OKLAHOMA", "OK");
        nameToShortMap.put("OREGON", "OR");
        nameToShortMap.put("PENNSYLVANIA", "PA");
        nameToShortMap.put("RHODE ISLAND", "RI");
        nameToShortMap.put("SOUTH CAROLINA", "SC");
        nameToShortMap.put("SOUTH DAKOTA", "SD");
        nameToShortMap.put("TENNESSEE", "TN");
        nameToShortMap.put("TEXAS", "TX");
        nameToShortMap.put("UTAH", "UT");
        nameToShortMap.put("VERMONT", "VT");
        nameToShortMap.put("VIRGINIA", "VA");
        nameToShortMap.put("WASHINGTON", "WA");
        nameToShortMap.put("WEST VIRGINIA", "WV");
        nameToShortMap.put("WISCONSIN", "WI");
        nameToShortMap.put("WYOMING", "WY");

        HashMap<String, PolygonOptions> ret = new HashMap<>();
        HashMap<String, ArrayList<LatLng>> tempList = new HashMap<>();

        for (String s : MapsApi.STATES) {
            tempList.put(s, new ArrayList<LatLng>());
        }

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document d = db.parse(act.getAssets().open("states.xml"));
            NodeList nodes = d.getElementsByTagName("state");
            //set up nodes for DOM XML parse
            for (int i = 0; i < nodes.getLength(); i++) {
                Node cur = nodes.item(i);
                if (cur.getNodeType() == Node.ELEMENT_NODE) {
                    Element curE = (Element) cur;
                    String name = nameToShortMap.get(curE.getAttribute("name").toUpperCase());

                    NodeList stateNode = curE.getElementsByTagName("point");
                    for (int j = 0; j < stateNode.getLength(); j++) {
                        tempList.get(name).add(new LatLng(
                                Double.parseDouble(((Element) (stateNode.item(j))).getAttribute("lat")),
                                Double.parseDouble(((Element) (stateNode.item(j))).getAttribute("lng"))
                        ));
                    }

                }

            }
        } catch (Exception e) {
            System.out.println("Doc Builder Failure!");
            e.printStackTrace();
        }


        //all things parsed, time to create the polygons
        for (String s : tempList.keySet()) {
            PolygonOptions p = new PolygonOptions();
            p.addAll(tempList.get(s));
            ret.put(s, p);
            //might need some more stuff in the options for colors, not sure how gmaps handles by default
        }

        loadedStatePoints = true;
        statePoints = ret;
        Log.e("polygons", "finished load");
        return ret;
    }


    public static double totalDistance(ArrayList<RideData> d) {
        double dist = 0;
        for (RideData data : d)
            dist += data.getDistance();
        return dist;
    }

    public static double totalCost(ArrayList<RideData> d) {
        double dist = 0;
        for (RideData data : d)
            dist += data.getCost();
        return dist;
    }
}
