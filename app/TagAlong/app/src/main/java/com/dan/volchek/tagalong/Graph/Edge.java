package com.dan.volchek.tagalong.Graph;

import com.dan.volchek.tagalong.activities.RideData;

public class Edge {
    public RideData data;
    Node endNode;
    Node startNode;
    double distance;

    public Edge(RideData d, Node startNode, Node endNode, double distance) {
        data = d;
        this.startNode=startNode;
        this.endNode = endNode;
        this.distance = distance;
    }
}
