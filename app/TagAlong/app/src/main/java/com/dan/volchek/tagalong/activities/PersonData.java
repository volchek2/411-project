package com.dan.volchek.tagalong.activities;


import android.net.Uri;

import java.io.File;

/**
 * Represents a row in the users table. - more specifically used to display people in
 * {@link com.dan.volchek.tagalong.activities.screens.RideViewActivity}.
 * <p>
 * NEEDS COMMENTS ATTACHED?
 */
public class PersonData {
    private String name;
    private String contactInfo;

    public PersonData(String n, String info) {
        name = n;
        contactInfo = info;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public String getUserName() {
        return name;
    }

}
