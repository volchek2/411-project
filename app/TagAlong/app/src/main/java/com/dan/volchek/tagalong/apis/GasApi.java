package com.dan.volchek.tagalong.apis;

import android.app.Activity;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.interfaces.GasResponse;
import com.dan.volchek.tagalong.interfaces.GotGas;
import com.dan.volchek.tagalong.requests.GasJsonObjectRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GasApi implements Response.ErrorListener, Response.Listener<JSONObject>, GasResponse {

    public void getGasPrice(String location, GotGas caller, Activity act_caller) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("s", location);
            GasJsonObjectRequest r = new GasJsonObjectRequest(Request.Method.POST,
                    "http://www.gasbuddy.com/Home/Search",
                    obj, this, this, caller, act_caller, this);
            Utils.directToQueue(r);
        } catch (JSONException e) {
            Log.e("Error Gas", "Couldn't create gas request");
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
    }

    @Override
    public void fancyResponse(JSONObject response, GotGas caller, Activity act_caller) {
        double price = stupid(response.toString());
        Log.e("gas:", price + "");
        caller.gotGasPrice(price);


    }

    private double stupid(String s) {
        ArrayList<Double> results = new ArrayList<>();
        for (int i = 0; i < s.length() - 3; i++) {
            int c_i = s.charAt(i);
            int c_i_2 = s.charAt(i + 2);
            int c_i_3 = s.charAt(i + 3);
            if (c_i >= 48 && c_i <= 57 && c_i_2 >= 48 && c_i_2 <= 57 && c_i_3 >= 48 && c_i_3 <= 57 &&
                    s.charAt(i + 1) == '.') {
                double pos = Double.parseDouble(s.substring(i, i + 4));
                if (pos > 1.00)
                    results.add(pos);
            }
        }
        if (results.size() == 0)
            return 0.0;
        double max = 0.0;
        for (double d : results)
            max += d;
        return max / results.size();
    }

    @Override
    public void fancyError(VolleyError error, GotGas caller, Activity act_caller) {
        if (error.getMessage() == null)
            Log.e("Error response", "MESSAGE WAS NULL");
        else
            Log.e("Error response", error.getMessage());
        error.printStackTrace();
        Utils.makeToast(act_caller, "Network Error");
        caller.gotGasPrice(0);
    }
}
