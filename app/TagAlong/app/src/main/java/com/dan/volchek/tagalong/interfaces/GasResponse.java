package com.dan.volchek.tagalong.interfaces;

import android.app.Activity;
import com.android.volley.VolleyError;
import org.json.JSONObject;

public interface GasResponse {
    void fancyResponse(JSONObject response, GotGas caller, Activity act_caller);

    void fancyError(VolleyError error, GotGas caller, Activity act_caller);
}
