package com.dan.volchek.tagalong.interfaces;

public interface GotGas {
    void gotGasPrice(double price);
}
