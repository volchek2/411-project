package com.dan.volchek.tagalong.activities.screens;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.VolleyErrorActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dmitriy on 3/3/2017.
 */
public class ConfirmPassActivity extends VolleyErrorActivity {
    private Button acceptButton;
    private Button denyButton;
    private String userDriver;
    private String userPass;
    private String startT;
    private String endT;


    @Override
    public void onResponse(JSONObject response) {
        //check for fails
        try {
            String status = response.getString("status");
            String authToken = response.getString("response");
            if (!status.equals("1")) {
                Utils.makeToast(this, authToken);
            } else {
                Utils.makeToast(this, authToken);
                finish();//on success
            }
        } catch (JSONException e) {
            Log.e("Response", "Data in wrong format?");
            e.printStackTrace();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        acceptButton = (Button) findViewById(R.id.ac_ride_accept);
        denyButton = (Button) findViewById(R.id.ac_ride_deny);
        //get ride from the intent
        //savedInstanceState.getStringArray("data");
        String authtoken = Utils.getDetails()[1];
        Intent i = getIntent();
        String[] intentData = i.getStringArrayExtra("data"); //not done yet
        userPass = intentData[0];
        userDriver = Utils.getDetails()[0];
        startT = intentData[1];
        endT = intentData[2];
        String tripName = intentData[3];

        ClickListener listener = new ClickListener();
        acceptButton.setOnClickListener(listener);
        denyButton.setOnClickListener(listener);
        ((TextView) findViewById(R.id.ac_trip_name)).setText(tripName);
        ((TextView) findViewById(R.id.ac_user_join)).setText(userPass);
        ((TextView) findViewById(R.id.ac_trip_details)).setText("Start: " + startT + " End: " + endT);
        /* request code, no purpose rn
        Map<String, String> params = new HashMap<>();
        params.put("request", "get_trip_info");
        params.put("authtoken", authtoken);
        params.put("username", userDriver);
        params.put("passenger_username", userPass);
        params.put("ride_start_time", startT);
        params.put("ride_end_time", endT);
        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Utils.constructRequest(request, this, this);
        */

        (findViewById(R.id.ac_user_join)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.startActivity(activity, ProfileActivity.class, new String[]{userPass}, false);
            }
        });

    }

    private void clickHelper(boolean isAccept) {
        Map<String, String> params = new HashMap<>();
        params.put("request", "join_ride");
        params.put("type", isAccept + "");
        params.put("username", userDriver);
        params.put("auth_token", Utils.getDetails()[1]);
        params.put("passenger_username", userPass);
        params.put("ride_start_time", startT);
        params.put("ride_end_time", endT);
        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Utils.constructRequest(request, this, this);

    }

    private class ClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (view == acceptButton) {
                clickHelper(true);
            } else if (view == denyButton) {
                clickHelper(false);
            }
        }
    }
}