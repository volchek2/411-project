package com.dan.volchek.tagalong.activities;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.screens.RideViewActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultiRideItemAdapter extends ArrayAdapter<ArrayList<RideData>> implements Response.ErrorListener, Response.Listener<JSONObject> {
    private Context c;
    private Activity act;

    public MultiRideItemAdapter(Context context, int resource, List<ArrayList<RideData>> objects) {
        super(context, resource, objects);
        c = context;
    }

    public void setActivity(Activity a) {
        act = a;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.multi_ride_item, null);
        }

        final ArrayList<RideData> p = getItem(position);

        if (p != null && p.size() != 0) {
            TextView header = (TextView) v.findViewById(R.id.multi_ride_item_header_1);
            TextView header2 = (TextView) v.findViewById(R.id.multi_ride_item_header_2);
            TextView header3 = (TextView) v.findViewById(R.id.ml_header_three);
            header3.setText("Distance: ~" + Utils.totalDistance(p) + " km - Cost: $" + Utils.totalCost(p));
            header2.setText(p.get(0).getStartTime() + " to " + p.get(p.size() - 1).getEndTime());
            header.setText(p.get(0).getStartLoc() + " to " + p.get(p.size() - 1).getEndLoc() + " - " + p.size() + " trip" + (p.size() == 1 ? "" : "s"));
            ListView list = (ListView) v.findViewById(R.id.multi_ride_item_list);

            //Code to make a list view scroll inside of another list view(scroll view) - taken from stackoverflow
            //http://stackoverflow.com/questions/18367522/android-list-view-inside-a-scroll-view
            list.setOnTouchListener(new View.OnTouchListener() {
                // Setting on Touch Listener for handling the touch inside ScrollView
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // Disallow the touch request for parent scroll on touch of child view
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                    RideData item = p.get(pos);
                    String[] data = new String[]{item.getUserName(), item.getStartTime(), item.getEndTime(),
                            item.getTripName(), item.getStartLoc(), item.getEndLoc(), "" + item.getCost()};
                    Utils.startActivity(c, RideViewActivity.class, data, false);
                }
            });

            Button mlButton = (Button) v.findViewById(R.id.ml_button);
            mlButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (RideData r : p) {
                        Map<String, String> params = new HashMap<>();
                        params.put("request", "join");
                        params.put("username", Utils.getDetails()[0]);
                        params.put("auth_token", Utils.getDetails()[1]);
                        params.put("ride_driver", r.getUserName());
                        params.put("ride_start_time", r.getStartTime());
                        params.put("ride_end_time", r.getEndTime());
                        params.put("ride_name", r.getTripName());

                        JSONObject request = (JSONObject) JSONObject.wrap(params);
                        Log.e("REQUEST", request.toString());

                        sendRequest(request);
                    }
                }
            });

            RideItemAdapter adapter = new RideItemAdapter(c, R.layout.ride_item, p);
            list.setAdapter(adapter);
        }

        return v;
    }

    private void sendRequest(JSONObject request) {
        Utils.constructRequest(request, this, this);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        if (act != null)
            Utils.makeToast(act, "Network Error joining rides.");
    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            String status = response.getString("status");
            if (status.equals("0")) {
                Utils.makeToast(act, response.getString("response"));
            } else {
                Utils.makeToast(act, "Sent request to join: " + response.getString("ride_name"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
