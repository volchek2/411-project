package com.dan.volchek.tagalong.activities.screens;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.dan.volchek.tagalong.R;
import com.dan.volchek.tagalong.Utils;
import com.dan.volchek.tagalong.activities.ToRidesActivity;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends ToRidesActivity {
    private Button logIn;

    private EditText usernameField;
    private EditText passwordField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginDetailsLocation = getFilesDir() + "/details.txt";
        //Utils.writeFile(loginDetailsLocation,"");
        String loginDetails = Utils.readFile(loginDetailsLocation);
        Log.e("LOGIN", "" +(loginDetails == null));
        if (loginDetails != null) {
            String[] details = loginDetails.split("\n");
            Log.e("LOGIN", "|" + loginDetails + "|");
            if (details.length == 2) {
                Utils.setDetails(details);
                if (details[0].length() != 0 && details[1].length() != 0) {
                    Utils.startActivity(activity, RidesActivity.class, null,
                            true);
                    return;
                }
            }
        }
        setContentView(R.layout.activity_login);

        logIn = (Button) findViewById(R.id.loginButton);
        usernameField = (EditText) findViewById(R.id.usernameEdit);
        passwordField = (EditText) findViewById(R.id.passwordEdit);

        TextView createAccount = (TextView) findViewById(R.id.signUpText);

        ClickListener listener = new ClickListener();
        logIn.setOnClickListener(listener);
        createAccount.setOnClickListener(listener);


    }

    private class ClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (view == logIn)
                validateLoginInfo();
            else
                Utils.startActivity(activity, SignupActivity.class, null, false);
        }
    }

    private void validateLoginInfo() {
        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();

        Map<String, String> params = new HashMap<>();
        params.put("request", "login");
        params.put("username", username);
        params.put("password", password);

        JSONObject request = (JSONObject) JSONObject.wrap(params);
        Log.e("REQUEST", request.toString());
        Utils.constructRequest(request, this, this);
        this.username = username;
    }
}
