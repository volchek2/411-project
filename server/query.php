	<?php

	include("/home/cstagalong/config/db_config.php");
	include("/home/cstagalong/config/random_compat/lib/random.php");

	$conn = new mysqli("localhost", $cfg_username, $cfg_password, $cfg_dbname);
	function exit_close_conn($message, $conn){
		if($conn){
			$conn->close();
		}
		exit(json_encode($message));
	}

	function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}

	function safe_query($query, $conn){
		$result = $conn->query($query);
		if(!$result)
			exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Database Query Failure","query"=>$query), $conn);
		return $result;
	}


	function generate_auth_token($username, $password, $request, $conn){
		$token = random_str(60);
		$query = "UPDATE users SET authtoken='".$token."' WHERE username ='".$username."' AND password ='".$password."';";
		$result = safe_query($query,$conn);
		exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$token), $conn);
	}

	function authenticate_account($username, $authtoken, $request, $conn){
		$query = "SELECT * FROM users WHERE username = '".$username."' AND authtoken = '".$authtoken."';";
		$result = safe_query($query,$conn);
		
		$result = $result->fetch_assoc();
		if(!$result)
			exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Account Authentication Failure"), $conn);	
	}

	function get_all_driven_by($driver,$username,$authtoken,$conn){
		$ret_arr = array();

		$query = "SELECT * FROM trips WHERE driver_username = '".$driver."';";
		$result = safe_query($query,$conn)->fetch_all();
		foreach($result as $row){
			$start_time=$row[0];
			$end_time=$row[1];

			$query = "SELECT passenger_username FROM passengers WHERE driver_username = '".$driver."' AND start_time = '".$start_time."' AND end_time='".$end_time."';";
			
			$passengers = safe_query($query,$conn)->fetch_all();
			$ret_pass=array();
			foreach($passengers as $passrow){
				array_push($ret_pass,$passrow[0]);
			}

			array_push($ret_arr,array("trip_info"=>$row,"passengers"=>$ret_pass));

		}

		return $ret_arr;
	}


	if ($conn->connect_error)
		exit(json_encode(array("status"=>"0",'request'=>$request,"response"=>"Database Connection Failure")));

	$post_data=null;

	if(count($_POST)!=0)
		$post_data=$_POST;
	else
		$post_data = json_decode(file_get_contents('php://input'),true);

	$request = null;
	$username = null;
	$password = null;
	$authtoken = null;
	$comment_username = null;
	$message=null;
	$time_left=null;

	$ride_driver=null;
	$ride_start_time=null;
	$ride_end_time=null;

	$contact_info=null;

	$type=null;
	$passenger_username=null;

	$above_avg=null;

	$ride_start_loc=null;
	$ride_end_loc=null;
	$ride_start_loc_lat=null;
	$ride_end_loc_lat=null;
	$ride_start_loc_long=null;
	$ride_end_loc_long=null;
	$ride_name=null;
	$ride_cost=null;
	$ride_distance=null;

	if($post_data["request"])
		$request = $post_data["request"];
	if($post_data["username"])
		$username = mysqli_escape_string($conn, trim($post_data["username"]));
	if($post_data["password"])
		$password = mysqli_escape_string($conn, trim($post_data["password"]));
	if($post_data["auth_token"])
		$authtoken = mysqli_escape_string($conn, trim($post_data["auth_token"]));
	if($post_data["comment_username"])
		$comment_username = mysqli_escape_string($conn, trim($post_data["comment_username"]));
	if($post_data["message"])
		$message = mysqli_escape_string($conn, trim($post_data["message"]));
	if($post_data["time_left"])
		$time_left = mysqli_escape_string($conn, trim($post_data["time_left"]));

	if($post_data["above_avg"])
		$time_left = mysqli_escape_string($conn, trim($post_data["above_avg"]));

	if($post_data["ride_driver"])
		$ride_driver = mysqli_escape_string($conn, trim($post_data["ride_driver"]));
	if($post_data["ride_start_time"])
		$ride_start_time = mysqli_escape_string($conn, trim($post_data["ride_start_time"]));
	if($post_data["ride_end_time"])
		$ride_end_time = mysqli_escape_string($conn, trim($post_data["ride_end_time"]));

	if($post_data["ride_start_loc"])
		$ride_start_loc = mysqli_escape_string($conn, trim($post_data["ride_start_loc"]));
	if($post_data["ride_end_loc"])
		$ride_end_loc = mysqli_escape_string($conn, trim($post_data["ride_end_loc"]));

	if($post_data["ride_distance"])
		$ride_distance = mysqli_escape_string($conn, trim($post_data["ride_distance"]));


	if($post_data["ride_start_loc_lat"])
		$ride_start_loc_lat = mysqli_escape_string($conn, trim($post_data["ride_start_loc_lat"]));
	if($post_data["ride_end_loc_lat"])
		$ride_end_loc_lat = mysqli_escape_string($conn, trim($post_data["ride_end_loc_lat"]));


	if($post_data["ride_start_loc_long"])
		$ride_start_loc_long = mysqli_escape_string($conn, trim($post_data["ride_start_loc_long"]));
	if($post_data["ride_end_loc_long"])
		$ride_end_loc_long = mysqli_escape_string($conn, trim($post_data["ride_end_loc_long"]));

	if($post_data["ride_name"])
		$ride_name = mysqli_escape_string($conn, trim($post_data["ride_name"]));

	if($post_data["contact_info"])
		$contact_info = mysqli_escape_string($conn, trim($post_data["contact_info"]));

	if($post_data["ride_cost"])
		$ride_cost = mysqli_escape_string($conn, trim($post_data["ride_cost"]));


	if($post_data["type"])
		$type = mysqli_escape_string($conn, trim($post_data["type"]));
	if($post_data["passenger_username"])
		$passenger_username = mysqli_escape_string($conn, trim($post_data["passenger_username"]));

	if($request){

		if($request=="login"){
			if($username && $password){
				$query = "SELECT authtoken FROM users WHERE username = '".$username."' AND password ='".$password."';";
				$result = safe_query($query,$conn);

				$result = $result->fetch_assoc();
				if(!$result){
					exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Username Password Combination Not Recognized"), $conn);	
				}else{
					$token = $result["auth_token"];
					if($token!=null){
						exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Already Logged In"), $conn);
					}else{
						generate_auth_token($username, $password, $request, $conn);	
					}	
				}
				
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Login Request"), $conn);
			}
		}

		if($request=="passenger_count"){
			if($username && $authtoken){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT COUNT(passenger_username), start_location, end_location FROM trips, passengers WHERE passengers.driver_username=trips.driver_username AND trips.start_time = passengers.start_time AND trips.end_time = passengers.end_time GROUP BY trips.start_location;";
				$result = safe_query($query,$conn)->fetch_all();
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$result), $conn);
			}
		}

		if($request=="all_trips"){
			if($username && $authtoken){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT * FROM trips;";
				$result = safe_query($query,$conn)->fetch_all();
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$result), $conn);
			}
		}

		if($request=="some_trips"){
			if($username && $authtoken){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT * FROM trips WHERE trips.start_time>='$ride_start_time' AND trips.end_time<='$ride_end_time'";
				if($above_avg=='true'){
					$query=$query." AND trips.name IN (SELECT trip_name FROM trips, comments WHERE trips.driver_username = comments.left_username AND comments.upvotes> (SELECT AVG(comments.upvotes) FROM comments, trips WHERE trips.driver_username=comments.left_username));";
				}else{
					$query=$query.";";
				}

				$result = safe_query($query,$conn)->fetch_all();
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$result), $conn);
			}
		}

		if($request=="logout"){
			if($username && $authtoken){
				authenticate_account($username,$authtoken,$request,$conn);

				$query = "UPDATE users SET authtoken=NULL WHERE username ='".$username."' AND authtoken ='".$authtoken."';";
				$result = safe_query($query,$conn);
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Logged Out"), $conn);
			}	

		}

		if($request=="signup"){
			if($username && $password && $contact_info){
				$query = "SELECT * FROM users WHERE username = '".$username."';";
				$result = safe_query($query,$conn);

				$result = $result->fetch_assoc();
				if($result){
					exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Account Already Exists"), $conn);	
				}else{
					$query = "INSERT INTO users VALUES('".$username."','".$contact_info."','".$password."', NULL);";
					safe_query($query,$conn);
					generate_auth_token($username,$password,$request,$conn);
				}
				
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Signup Request"), $conn);
			}
		}

		if($request=="driving"){
			if($username && $authtoken){
				authenticate_account($username,$authtoken,$request,$conn);
				$result = get_all_driven_by($username,$username,$authtoken,$conn);

				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$result),$conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Driving Request"), $conn);
			}
		}

		if($request=="join_ride"){
			if($username && $authtoken && $passenger_username && $ride_start_time && $ride_end_time && $type){
				authenticate_account($username,$authtoken,$request,$conn);
				if($type=="true"){
					$query = "INSERT INTO passengers VALUES ('".$username."', '".$ride_start_time."','".$ride_end_time."','".$passenger_username."');";
					safe_query($query,$conn);

					$query = "DELETE FROM pending_rides WHERE driver_username='".$username."' AND passenger_username='".$passenger_username."' AND start_time='".$ride_start_time."' AND end_time='".$ride_end_time."';";
					safe_query($query,$conn);
					exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Request Accepted"), $conn);
				}

				$query = "DELETE FROM pending_rides WHERE driver_username='".$username."' AND passenger_username='".$passenger_username."' AND start_time='".$ride_start_time."' AND end_time='".$ride_end_time."';";
				safe_query($query,$conn);
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Request Deleted"), $conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Accept Join Request"), $conn);
			}
		}

		if($request=="join"){
			if($username && $authtoken && $ride_driver && $ride_start_time && $ride_end_time){
				authenticate_account($username,$authtoken,$request,$conn);
				
				$query = "INSERT INTO pending_rides VALUES ('".$ride_driver."', '".$username."','".$ride_start_time."','".$ride_end_time."');";
				safe_query($query,$conn);
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Sent Request To Join Ride","ride_name"=>$ride_name), $conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Join Ride"), $conn);
			}

		}

		if($request=="leave"){
			if($username && $authtoken && $ride_driver && $ride_start_time && $ride_end_time){
				authenticate_account($username,$authtoken,$request,$conn);
				if($username != $ride_driver){
					$query = "DELETE FROM passengers WHERE driver_username='".$username."' AND passenger_username='".$passenger_username."' AND start_time='".$ride_start_time."' AND end_time='".$ride_end_time."';";
					safe_query($query,$conn);
					exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Left Ride"), $conn);
				}else{
					$query = "DELETE FROM passengers WHERE driver_username='".$username."' AND start_time='".$ride_start_time."' AND end_time='".$ride_end_time."';";
					safe_query($query,$conn);
					$query = "DELETE FROM trips WHERE driver_username='".$username."' AND start_time='".$ride_start_time."' AND end_time='".$ride_end_time."';";
					safe_query($query,$conn);
					exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Ride Deleted"), $conn);
				}
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Join Ride"), $conn);
			}

		}

		if($request=="find_rides"){
			if($username && $authtoken && $ride_start_time && $ride_end_time && $ride_start_loc && $ride_end_loc && $ride_cost){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT * from trips WHERE start_time='".$ride_start_time."' AND end_time='".$ride_end_time."' AND start_location='".$ride_start_loc."' AND end_location = '".$ride_end_loc."' and cost<= ".$ride_cost.";";
				$result = safe_query($query,$conn)->fetch_all();
				
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$result),$conn);
				
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Join Ride"), $conn);
			}

		}

		if($request=="create_ride"){
			if($username && $authtoken && $ride_start_time && $ride_end_time && $ride_start_loc && $ride_end_loc && $ride_cost && $ride_name){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT * FROM trips WHERE driver_username = '".$username."' AND start_time='".$ride_start_time."' AND end_time='".$ride_end_time."';";
				$result = safe_query($query,$conn)->fetch_all();
				if(count($result)!=0)
					exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"That Trip Already Exists"), $conn);
				$query = "INSERT INTO trips VALUES('".$username."', '".$ride_start_time."', '".$ride_end_time."', '".$ride_name."', '".$ride_start_loc."', '".$ride_end_loc."', ".floatval($ride_start_loc_lat).", ".floatval($ride_start_loc_long).", ".floatval($ride_end_loc_lat).", ".floatval($ride_end_loc_long).", ".floatval($ride_cost).", ".floatval($ride_distance).");";
				$result = safe_query($query,$conn);
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Ride Created"), $conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Create Ride"), $conn);
			}
		}

		if($request=="edit_ride"){
			if($username && $authtoken && $ride_start_time && $ride_end_time && $ride_cost && $ride_name){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT * FROM trips WHERE driver_username = '".$username."' AND start_time='".$ride_start_time."' AND end_time='".$ride_end_time."';";
				$result = safe_query($query,$conn)->fetch_all();
				if(count($result)==0)
					exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Trip Does Not Exist"), $conn);
				$query = "UPDATE trips SET trip_name='".$ride_name."', cost=".$ride_cost." WHERE driver_username = '".$username."' AND start_time='".$ride_start_time."' AND end_time='".$ride_end_time."';";
				$result = safe_query($query,$conn);
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Ride Created"), $conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Edit Ride"), $conn);
			}
		}

		if($request =='get_pending'){
			if($username && $authtoken){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT trip_name, pending_rides.driver_username, pending_rides.passenger_username, pending_rides.start_time, pending_rides.end_time FROM pending_rides, trips WHERE pending_rides.driver_username='".$username."' AND pending_rides.driver_username=trips.driver_username AND pending_rides.start_time=trips.start_time AND pending_rides.end_time=trips.end_time;";
				$result = safe_query($query,$conn)->fetch_all();
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$result),$conn);
			}
		}

		if($request=="passengers"){
			if($username && $authtoken && $ride_driver && $ride_start_time && $ride_end_time){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT passenger_username FROM passengers WHERE driver_username = '".$ride_driver."' AND start_time='".$ride_start_time."' AND end_time='".$ride_end_time."';";
				$result = safe_query($query,$conn)->fetch_all();
				$ret_arr = array();
				foreach($result as $row){
					$query = "SELECT contact_info from users WHERE username = '".$row[0]."';";
					$res = safe_query($query,$conn)->fetch_assoc();
					$temp = array();
					array_push($ret_arr,array("username"=>$row[0],"contact_info"=>$res['contact_info']));
				}

				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$ret_arr),$conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Passengers Request"), $conn);
			}
		}

		if($request=="riding"){
			if($username && $authtoken){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT driver_username FROM passengers WHERE passenger_username = '".$username."';";
				$result = safe_query($query,$conn)->fetch_all();
				$ret_arr = array();
				foreach($result as $row){
					$driver=$row[0];
					$ride = get_all_driven_by($driver,$username,$authtoken,$conn);
					foreach($ride as $r){
						array_push($ret_arr,$r);
					}
				}
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$ret_arr),$conn);

			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Riding Request"), $conn);
			}
		}

		if($request=="get_comments_about"){
			if($username && $authtoken && $comment_username){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT * FROM comments WHERE about_username = '".$comment_username."' ORDER BY comments.upvotes DESC;";
				$result = safe_query($query,$conn)->fetch_all();
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$result),$conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Get Comments About Request"), $conn);
			}
		}

		if($request=="get_comments_by"){
			if($username && $authtoken && $comment_username){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT * FROM comments WHERE left_username = '".$comment_username."';";
				$result = safe_query($query,$conn)->fetch_all();
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$result),$conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Get Comments By Request"), $conn);
			}
		}

		if($request=="contact_info"){
			if($username && $authtoken && $comment_username){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "SELECT contact_info FROM users WHERE username = '".$comment_username."';";
				$result = safe_query($query,$conn)->fetch_assoc();
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>$result),$conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Get Contact Info Request"), $conn);
			}
		}


		if($request=="leave_comment"){
			if($username && $authtoken && $comment_username){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "INSERT INTO comments (time_left, left_username, about_username, message) VALUES('".$time_left."', '".$username."', '".$comment_username."', '".$message."');";
				$result = safe_query($query,$conn);
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Left Comment"),$conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Leave Comment Request"), $conn);
			}
		}

		if($request=="delete_comment"){
			if($username && $authtoken && $time_left){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "DELETE FROM comments WHERE left_username='".$username."' AND time_left='".$time_left."';";
				safe_query($query,$conn);

				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>"Deleted Comment"),$conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Delete Comment Request"), $conn);
			}
		}

		if($request == "upvote_comment"){
			if($username && $authtoken && $time_left && $comment_username ){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "UPDATE comments SET upvotes = upvotes + 1 WHERE left_username='".$comment_username."' AND time_left='".$time_left."';";
				safe_query($query,$conn);

				$query = "SELECT upvotes FROM comments WHERE left_username='".$comment_username."' AND time_left='".$time_left."';";
				$result = safe_query($query,$conn)->fetch_assoc()["upvotes"];
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>array("username"=>$comment_username,"time_left"=>$time_left,"votes"=>$result)),$conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Upvote Comment Request"), $conn);
			}
		}

		if($request == "downvote_comment"){
			if($username && $authtoken && $time_left && $comment_username ){
				authenticate_account($username,$authtoken,$request,$conn);
				$query = "UPDATE comments SET upvotes = upvotes - 1 WHERE left_username='".$comment_username."' AND time_left='".$time_left."';";
				safe_query($query,$conn);

				$query = "SELECT upvotes FROM comments WHERE left_username='".$comment_username."' AND time_left='".$time_left."';";
				$result = safe_query($query,$conn)->fetch_assoc()["upvotes"];
				exit_close_conn(array("status"=>"1",'request'=>$request,"response"=>array("username"=>$comment_username,"time_left"=>$time_left,"votes"=>$result)),$conn);
			}else{
				exit_close_conn(array("status"=>"0",'request'=>$request,"response"=>"Bad Parameters for Downvote Comment Request"), $conn);
			}
		}
		
	}else{
		exit_close_conn(array("status"=>"0",'request'=>'bad',"response"=>"Bad Request"), $conn);
	}
	?> 